package com.epam.at.hw18.services;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SerialisableEmployee {
    private Connection connection;
    private Statement statement;

    public SerialisableEmployee() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:/epam/tanya_dantsova/src/com/epam/training/hw9/src/main/resources/db/db_for_m4_l3.db");
    }

    public void addEmployee(String line) throws SQLException {
        String[] strings = line.trim().split("\\s+");
        String firstName = strings[0];
        String lastname = strings[1];
        statement = connection.createStatement();
        statement.executeUpdate("INSERT INTO Employee(firstname, lastname) " +
                "values ('" + firstName + "', '" + lastname + "')");
        System.out.println("Employee added");
    }

    public void removeEmployee(Integer id) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM Employee WHERE id = " + id);
        System.out.println("Employee removed");
    }

    public List <Employee> getAll() throws SQLException {
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from Employee");
        List <Employee> employeeList = new ArrayList <Employee>();
        while (resultSet.next()) {
            Employee employee = new Employee();
            employee.setId(Integer.parseInt(resultSet.getString("id")));
            employee.setFirstname(resultSet.getString(2));
            employee.setLastname(resultSet.getString(3));
            employeeList.add(employee);
        }
        return employeeList;
    }

    public void closeConections() throws SQLException {
        connection.close();
        statement.close();
    }
}