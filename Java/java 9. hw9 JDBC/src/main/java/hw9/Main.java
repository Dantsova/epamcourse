package com.epam.at.hw18.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
        hw9.SerialisableEmployee serialisableEmployee = new hw9.SerialisableEmployee();
        int number = 0;

        while (true) {
            System.out.println("");
            System.out.println("Menu:");
            System.out.println("1. Add new employee.");
            System.out.println("2. Remove employee by ID.");
            System.out.println("3. Output of all employees.");
            System.out.println("4. EXIT.");
            System.out.println("____________________________");
            System.out.print("Enter the menu item: ");
            Scanner scanner = new Scanner(System.in);
            try {
                number = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException ex) {
                number = 0;
            }
            switch (number) {
                case 1: {
                    System.out.println("Input data of employee: ");
                    System.out.println("firstname lastname");
                    System.out.println("-------------------");
                    String line = scanner.nextLine();
                    try {
                        serialisableEmployee.addEmployee(line);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage() + "Incorrect data input");
                    }
                    break;
                }
                case 2:
                    System.out.print("Enter employee Id to delete: ");
                    int id = scanner.nextInt();
                    try {
                        serialisableEmployee.removeEmployee(id);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 3:
                    List <hw9.Employee> list = serialisableEmployee.getAll();
                    for (hw9.Employee empl : list) {
                        System.out.println(empl);
                    }
                    break;
                case 4:
                    System.exit(0);
                    serialisableEmployee.closeConections();
                default:
                    System.out.println("Incorrect input menu item");
            }
        }
    }
}