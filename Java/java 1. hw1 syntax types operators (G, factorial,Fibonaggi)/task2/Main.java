//программа принимает на вход три целых числа
//   1. algorithmId - тип алгоритма
//     1. вычисление ряда чисел Фибоначчи;
//     2. вычисление факториала
//   2. loopType - тип циклов, которые нужно использовать:
//     1. цикл while
//     2. цикл do-while
//     3. цикл for
//   3. n - параметр, передаваемый в алгоритм.

package com.epam.training.hw1.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Input id algorithm (1 - Fibonacci numbers, 2 - factorial): ");
        int algorithmId = in.nextInt();
        System.out.print("Input loop type (1 - while, 2 - do-while, 3 - for): ");
        int loopType = in.nextInt();
        System.out.print("Input n: ");
        int n = in.nextInt();

        long result = 0;
        switch (algorithmId) {
            case 1:
                result = Fibonacci.calculate(loopType, n);
                break;
            case 2:
                result = Factorial.calculate(loopType, n);
                break;
            default: {
                throw new RuntimeException(" Invalid algorithm Id (id algorithm: 1 - Fibonacci numbers or 2 - factorial) ");
            }
        }
        System.out.println("Result = " + result);
    }
}