// вычисление факториала, реализовано с помощью разных циклов;
//n - параметр, передаваемый в алгоритм.

package com.epam.training.hw1.task2;

public class Factorial {

    public static long calculate(int loopType, int n) {
        switch (loopType) {
            case 1:
                return whileLoop(n);
            case 2:
                return doWhileLoop(n);
            case 3:
                return forLoop(n);
            default:
        }
        return 0;
    }

    private static long whileLoop(int n) {
        int resultFactorial = 1;
        int i = 1;
        while (i <= n) {
            resultFactorial = resultFactorial * i;
            i++;
        }
        return resultFactorial;
    }

    private static long doWhileLoop(int n) {
        int resultFactorial = 1;
        int i = 1;
        do {
            resultFactorial = resultFactorial * i;
            i++;
        }
        while (i <= n);
        return resultFactorial;
    }

    private static long forLoop(int n) {
        int resultFactorial = 1;
        for (int i = 1; i <= n; i++) {
            resultFactorial = resultFactorial * i;
        }
        return resultFactorial;
    }
}
