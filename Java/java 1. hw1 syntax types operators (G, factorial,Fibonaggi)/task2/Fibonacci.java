// вычисление ряда чисел Фибоначчи, реализовано с помощью разных циклов;
// n - параметр, передаваемый в алгоритм.

package com.epam.training.hw1.task2;

public class Fibonacci {

    public static long calculate(int loopType, int n) {
        switch (loopType) {
            case 1:
                return whileLoop(n);
            case 2:
                return doWhileLoop(n);
            case 3:
                return forLoop(n);
            default:
                throw new RuntimeException("Incorrect data entered (loop type: 1 - while, 2 - do-while, 3 - for) ");
        }
    }

    private static long whileLoop(int n) {
        int i = 3;
        long f1 = 1;
        long f2 = 1;
        long resultFibonacci = 1;
        while (i <= n) {
            resultFibonacci = f1 + f2;
            f1 = f2;
            f2 = resultFibonacci;
            i++;
        }
        return resultFibonacci;
    }

    private static long doWhileLoop(int n) {
        int i = 2;
        long f1 = 0;
        long f2 = 1;
        long resultFibonacci = 0;
        do {
            resultFibonacci = f1 + f2;
            f1 = f2;
            f2 = resultFibonacci;
            i++;
        }
        while (i <= n);
        return resultFibonacci;
    }

    private static long forLoop(int n) {
        long resultFibonacci = 1;
        long f1 = 0;
        long f2 = 1;
        for (int i = 1; i < n; i++) {
            resultFibonacci = f1 + f2;
            f1 = f2;
            f2 = resultFibonacci;
        }
        return resultFibonacci;
    }
}
