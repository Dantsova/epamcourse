// Программа принимает на вход четыре числа, зн-ия передавать через аргументы командной строки;
// Вывести на экран значение числа G;
// Для получения значения «пи» используйте константу: java.lang.Math.PI;
// Run-> Edit Configuration -> Program arguments: ...;

package com.epam.training.hw1.task1;
import java.util.HashMap;


public class CommandLine {

    public static void main(String[] args) throws ArithmeticException {
        int a = 0;
        int p = 0;
        double m1 = 0;
        double m2 = 0;
        double G = 0;
        boolean b = 4;
        System.out.println(b);

        try {
            a = Integer.parseInt(args[0]);
            p = Integer.parseInt(args[1]);
            m1 = Double.parseDouble(args[2]);
            m2 = Double.parseDouble(args[3]);
        } catch (NumberFormatException e) {
            throw new RuntimeException("Incorrect data entered");
        }
        if ((p == 0) || ((m1 + m2) == 0)) {
            throw new ArithmeticException("Divide by 0");
        }

        G = (4 * Math.pow(Math.PI, 2) * Math.pow(a, 3)) / (Math.pow(p, 2) * (m1 + m2));
        System.out.println("G = " + G + " or G = " + String.format("%.2f", G).replace(',', '.'));
    }
}


