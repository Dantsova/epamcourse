package com.epam.training.hw4.task2;

import java.util.Arrays;

public class BubbleSort implements Sorter {

    @Override
    public int[] sort(int[] array) {
        boolean isSorted = false;
        int buf = 0;

        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    buf = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buf;
                    isSorted = false;
                }
            }
        }

        System.out.println(Arrays.toString(array));
        return array;
    }
}

