package com.epam.training.hw4.task2;

 public interface Sorter {
    int[] sort(int[] array);
}