package com.epam.training.hw4.task2;

import java.util.Scanner;

public class SortingContext {
    Sorter sortStrategy;

    public SortingContext(Sorter sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public int[] execute(int[] array) {
        return sortStrategy.sort(array);
    }
}

