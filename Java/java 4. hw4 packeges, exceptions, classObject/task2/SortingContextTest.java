package com.epam.training.hw4.task2;

import org.junit.Assert;
import org.junit.Test;

public class SortingContextTest {
    @Test
    public void testSortByBubbleStrategy() {
        SortingContext sortingContext = new SortingContext(new BubbleSort());
        int[] mas = sortingContext.execute(new int[]{1, 8, 6, 9, 7});
        Assert.assertArrayEquals(new int[]{1, 6, 7, 8, 9}, mas);
    }

    @Test
    public void testSortBySelectionStrategy() {
        SortingContext sortingContext = new SortingContext(new SelectionSort());
        int[] mas = sortingContext.execute(new int[]{1, 8, 6, 9, 7});
        Assert.assertArrayEquals(new int[]{1, 6, 7, 8, 9}, mas);
    }
}
