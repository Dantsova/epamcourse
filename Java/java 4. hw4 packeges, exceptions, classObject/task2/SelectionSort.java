package com.epam.training.hw4.task2;

import java.util.Arrays;

class SelectionSort implements Sorter {

    @Override
    public int[] sort(int[] array) {
        int min, buf;

        for (int index = 0; index < array.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < array.length; scan++) {
                if (array[scan] < array[min])
                    min = scan;
            }
            buf = array[min];
            array[min] = array[index];
            array[index] = buf;
        }
        System.out.println(Arrays.toString(array));
        return array;
    }
}

