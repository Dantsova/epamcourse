package com.epam.training.hw4.task1;

public class Atm {
    private Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public void decreaseBalance(double sumToDecrease) {
        card.decreaseBalance(sumToDecrease);
    }

    public void increaseBalance(double sumToIncrease) {
        card.increaseBalance(sumToIncrease);
    }
}
