package com.epam.training.hw4.task1;

public class CreditCard extends Card {

    @Override
    public void decreaseBalance(double sumToDecrease) {
        setBalance(getBalance() - sumToDecrease);
    }

    public CreditCard(String ownerName, double balance) {
        super(ownerName, balance);
    }

    public CreditCard(String ownerName) {
        super(ownerName);
    }
}
