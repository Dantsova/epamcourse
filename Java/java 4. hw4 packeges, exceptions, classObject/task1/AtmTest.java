package com.epam.training.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

public class AtmTest {

    @Test
    public void testIncreaseBalanceCreditCard() {
        Card card = new CreditCard("Tom", 100);
        Atm atm = new Atm(card);
        atm.increaseBalance(50);
        Assert.assertEquals(150, card.getBalance(), 0);
    }

    @Test
    public void testIncreaseBalanceDebitCard() {
        Card card = new DebitCard("Tim", 100);
        Atm atm = new Atm(card);
        atm.increaseBalance(50);
        Assert.assertEquals(150, card.getBalance(), 0);
    }

    @Test
    public void testDecreaseBalanceCreditCard() {
        Card card = new CreditCard("Bob", 100);
        Atm atm = new Atm(card);
        atm.decreaseBalance(150);
        Assert.assertEquals(-50, card.getBalance(), 0);
    }

    @Test
    public void testDecreaseBalanceDebitCard() {
        Card card = new DebitCard("Bob", 100);
        Atm atm = new Atm(card);
        atm.decreaseBalance(50);
        Assert.assertEquals(50, card.getBalance(), 0);
    }

    @Test(expected = RuntimeException.class)
    public void testDecreaseBalanceDebitCardWithSumToReduseGreaterThanBalance() {
        DebitCard card = new DebitCard("Bom", 100);
        Atm atm = new Atm(card);
        atm.decreaseBalance(150);
        Assert.assertEquals(100, card.getBalance(), 0);
    }
}

