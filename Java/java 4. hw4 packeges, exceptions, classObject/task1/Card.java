package com.epam.training.hw4.task1;

public class Card {
    private double balance = 0;
    private String ownerName;

    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setBalance(double startBalance) {
        balance = startBalance;
    }

    public double getBalance() {
        return balance;
    }

    public double getBalanceWithConversion(float conversion) {
        return getBalance() * conversion;
    }

    public void increaseBalance(double sumToIncrease) {
        balance = balance + sumToIncrease;
    }

    public void decreaseBalance(double sumToDecrease) {
        if (balance < sumToDecrease) {
            throw new RuntimeException("You don't have enough balance");
        } else {
            balance = balance - sumToDecrease;
        }
    }
}

