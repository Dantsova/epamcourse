package com.epam.training.hw4.task1;

public class DebitCard extends Card {

    public DebitCard(String ownerName, double balance) {
        super(ownerName, balance);
    }

    public DebitCard(String ownerName) {
        super(ownerName);
    }
}

