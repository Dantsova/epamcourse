package com.epam.training.hw8;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Task8 {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        SerializableEmployee employee = new SerializableEmployee();
        int numberToRemove = 0;
        String[] strings;
        int number = 0;

        while (true) {
            System.out.println("___________________________");
            System.out.println("Menu:");
            System.out.println("1. Add new employee.");
            System.out.println("2. Remove employee by ID.");
            System.out.println("3. Output of all employees.");
            System.out.println("4. Save data to file and EXIT.");
            System.out.println("____________________________");
            System.out.print("Enter the menu item: ");
            Scanner scanner = new Scanner(System.in);
            try {
                number = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException ex) {
                number = 0;
            }
            switch (number) {
                case 1: {
                    System.out.println("Input data of employee: ");
                    System.out.println("(id firstname lastname)");
                    String line = scanner.nextLine();
                    try {
                        employee.addEmployee(line);
                    } catch (Exception ex) {
                        System.out.println("Incorrect data input");
                    }
                    break;
                }
                case 2:
                    int id = scanner.nextInt();
                    employee.removeEmployee(id);
                    break;
                case 3:
                    List <Employee> list = employee.getAll();
                    for (Employee empl : list) {
                        System.out.println(empl);
                    }
                    break;
                case 4:
                    employee.writeToFile();
                    System.exit(0);
                default:
                    System.out.println("Incorrect input menu item");
            }
        }
    }
}

