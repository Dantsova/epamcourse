package com.epam.training.hw8;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static javax.management.Query.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;

public class SerializableEmployeeTest {

    @Test
    public void addEmployeeTest() throws IOException, ClassNotFoundException {
        Employee expectedEmployee = new Employee(1, "V", "D");
        SerializableEmployee serializableEmployeeExpected = new SerializableEmployee();
        serializableEmployeeExpected.addEmployee(expectedEmployee.toString());
        serializableEmployeeExpected.writeToFile();

        SerializableEmployee serializableEmployeeActual = new SerializableEmployee();
        List<Employee> employeeList = serializableEmployeeActual.getAll();

        assertThat(employeeList, hasItem(expectedEmployee));
    }
/*
    @Test
    public void removeEmployeeTest() throws IOException, ClassNotFoundException {
        Employee expectedEmployee1 = new Employee(1, "V", "D");
        Employee expectedEmployee2 = new Employee(2, "W", "F");
        SerializableEmployee serializableEmployeeExpected = new SerializableEmployee();
        serializableEmployeeExpected.addEmployee(expectedEmployee1.toString());
        serializableEmployeeExpected.addEmployee(expectedEmployee2.toString());
        serializableEmployeeExpected.writeToFile();

        SerializableEmployee serializableEmployeeActual = new SerializableEmployee();
        serializableEmployeeActual.removeEmployee(1);
        List <Employee> employeeList = serializableEmployeeActual.getAll();

        assertThat(employeeList, not(hasItem(expectedEmployee1)));
    }
*/
}


