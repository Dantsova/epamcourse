package com.epam.training.hw8;

import java.io.*;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SerializableEmployee implements Serializable {
    static final long SerialVersionUID = -5882008189407597953L;
    private static final String NAME_FILE = FileSystems.getDefault().getPath("tableEmployees_hw8.ser").toString();
    private Map <Integer, Employee> employeeMap = new HashMap();

    public SerializableEmployee() throws IOException, ClassNotFoundException {
        File file = new File(NAME_FILE);
        if (!file.exists()) {
            file.createNewFile();
        }
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            Employee employeeFromFile;
            while (true) {
                employeeFromFile = (Employee) ois.readObject();
                if (employeeFromFile != null) {
                    employeeMap.put(employeeFromFile.getId(), employeeFromFile);
                }
            }
        } catch (EOFException e) {
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void addEmployee(String line) throws IOException {
        String[] strings = line.trim().split("\\s+");
        Integer id = Integer.parseInt(strings[0]);
        String firstName = strings[1];
        String lastname = strings[2];
        Employee employeeToFile;
        employeeToFile = new Employee(id, firstName, lastname);
        if (employeeMap.containsKey(id))
            System.out.println("Employee with this ID already exists");
        else {
            employeeMap.put(employeeToFile.getId(), employeeToFile);
        }
    }

    public void removeEmployee(int id) throws IOException {
        if (employeeMap.get(id) != null) {
            employeeMap.remove(id);
        } else System.out.println("This ID is not saved to table, try again...");
    }

    public List <Employee> getAll() throws IOException {
        List <Employee> employeeList = new ArrayList <>();
        for (Map.Entry <Integer, Employee> employeeMap : employeeMap.entrySet()) {
            employeeList.add(employeeMap.getValue());
        }
        return employeeList;
    }

    public void writeToFile() throws IOException {
        try (FileOutputStream fos = new FileOutputStream(NAME_FILE, false);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            for (Map.Entry <Integer, Employee> employeeMap : employeeMap.entrySet()) {
                oos.writeObject(employeeMap.getValue());
            }
            System.out.println("Data has been added to the file " + NAME_FILE);
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
    }
}