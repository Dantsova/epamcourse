package hw6;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * Сlass for counting the number of words by the first letter in the text has saved in the file
 *
 * @author TDantsova
 */
public class Text {
    public static void main(String[] args) throws IOException {
        String pathName = ".\\src\\main\\resources\\text.txt";
        String string = null;
        try {
            string = new String(Files.readAllBytes(Paths.get(pathName)));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        Text.outputStatisticsWords(getOnlyEnglishWords(string));
    }

    public static String getOnlyEnglishWords(String string) {
        string = string
                .toLowerCase()
                .replaceAll("(?u)[^a-z]", " ")
                .trim()
                .replaceAll("\\s+", " ");
        return string;
    }

    private static void outputStatisticsWords(String string) {
        Set<String> setWords = new TreeSet<String>(Arrays.asList(string.split(" ")));
        char firstLetter = '1';
        for (String element : setWords) {
            if (element.charAt(0) != firstLetter) {
                System.out.println(element.toUpperCase().charAt(0) + ": ");
                firstLetter = element.charAt(0);
            }
            int count = (string.split(element, -1).length) - 1;
            System.out.println("  " + element + " - " + count);
        }
    }
}
