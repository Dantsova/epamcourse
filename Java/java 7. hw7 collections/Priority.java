// Программа определяет (с помощью сообщений) порядок, в котором осуществляются след.действия:
// - объявление статической переменной,
// - объявление «обычной» переменной класса,
// - пару инициализационных блоков,
// - пару статических блоков,
// - создание в методе main ссылочной  переменной (без инициализации),
// - присвоение упомянутой ссылочной переменной ссылки на объект.

public class Priority {
    static String a;
    static int b;
    int c = 0;
    boolean d;

    {
        c = 10;
        System.out.println("Initialization block 1 completed.");
    }

    {
        d = true;
        System.out.println("Initialization block 2 completed.");
    }

    static {
        a = "text";
        System.out.println("Static block 1 completed.");
    }

    static {
        b = 6;
        System.out.println("Static block 2 completed.");
    }

    public Priority() {
        System.out.println("Constructor completed.");
    }

    public static void main(String[] args) {

        Priority e;

        e = new Priority();
        Priority e1 = new Priority();
        System.out.println("Static method completed.");
    }
}
