// программа выясняет значения по умолчанию примитивных и ссылочных типов.

package com.epam.training.hw2.task1;

public class Initializer {
    static byte a;
    static short b;
    static int c;
    static long d;
    static char e;
    static float f;
    static double g;
    static boolean i;
    static SomeClass someClass;

    public static void main(String[] args) {

        System.out.println("defaults byte = " + a);
        System.out.println("defaults short = " + b);
        System.out.println("defaults int = " + c);
        System.out.println("defaults long = " + d);
        System.out.println("defaults char = " + e);
        System.out.println("defaults float = " + f);
        System.out.println("defaults double = " + g);
        System.out.println("defaults boolean = " + i);
        System.out.println("defaults someClass = " + someClass);
    }
}
