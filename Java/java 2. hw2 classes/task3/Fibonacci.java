package  main.java.Fibonacci;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {
    private static Map <Integer, Long> fibonacciValues = new HashMap <>();

    public static long evaluate(int n) {
        long f1 = 0;
        long f2 = 0;
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        } else {
            if (fibonacciValues.containsKey(n - 2)) {
                f1 = fibonacciValues.get(n - 2);
            } else {
                f1 = evaluate(n - 2);
                fibonacciValues.put(n - 2, f1);
            }
            if (fibonacciValues.containsKey(n - 1)) {
                f2 = fibonacciValues.get(n - 1);
            } else {
                f2 = evaluate(n - 1);
                fibonacciValues.put(n - 1, f2);
            }
            long nextValue = f1 + f2;
            return nextValue;
        }
    }
}



