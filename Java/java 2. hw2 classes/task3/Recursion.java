// программа, определяющая какое по счету число Фибоначчи может быть вычислено правильно
// - для типа int? (32-разрядное целое число со знаком, от -2 147 483 648 до 2 147 483 647)
// - для типа long?* (64-разрядное целое число со знаком, От -9 223 372 036 854 775 808 до 9 223 372 036 854 775 807)

import java.util.Scanner;
import main.java.*;

class Recursion {
    static int position = 0;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input type ID (1 - int, 2 - long ) : ");
        int typeId = in.nextInt();
        int i = 1;
        switch (typeId) {
            case 1: {
                limitInt();
            }
            break;
            case 2: {
                limitLong();
            }
            break;
        }
        System.out.println("Position: " + position);
    }

    private static int limitInt() {
        long result;
        int i = 0;
        do {
            result = Fibonacci.evaluate(i);
            if (result > Integer.MAX_VALUE) {
                position = i;
                break;
            }
            i++;
        } while (result < Integer.MAX_VALUE);
        return position;
    }

    private static int limitLong() {
        long result;
        int i = 0;
        do {
            result = Fibonacci.evaluate(i);
            if (result < 0) {
                position = i - 1;
                break;
            }
            i++;
        } while (result >= 0);
        return position;
    }
}
