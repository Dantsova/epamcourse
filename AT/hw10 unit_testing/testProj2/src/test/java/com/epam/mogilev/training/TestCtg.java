package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCtg {
    Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = {"positive", "failTests"})
    public void testTg_SameValue1() {
        double actualResult = calculator.ctg(Math.PI / 4);
        Assert.assertEquals(actualResult, 1.0);
    }

    @Test(groups = {"positive", "failTests"})
    public void testTg_SameValue2() {
        double actualResult = calculator.ctg(Math.PI / 2);
        Assert.assertEquals(actualResult, 0.0);
    }

    @Test(groups = {"negative", "failTests"})
    public void testTg_Asymptote() {
        double actualResult = calculator.ctg(0);
        Assert.assertEquals(actualResult, "Attempting to take ctg() at the asymptote point");
    }
}