package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestSub {
    Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = "positive")
    public void testSub_LongNumbers() {
        long actualResult = calculator.sub(100, 10);
        Assert.assertEquals(actualResult, 90);
    }

    @Test(groups = "positive")
    public void testSub_DoubleNumbers() {
        double actualResult = calculator.sub(100.10, 0.10);
        Assert.assertEquals(actualResult, 100.00);
    }

    @Test(groups = "positive")
    public void testSub_NumbersWithNegativeResult() {
        long actualResult = calculator.sub(10, 100);
        Assert.assertEquals(actualResult, -90);
    }

    @Test(groups = "positive")
    public void testSub_NegativeNumbers() {
        long actualResult = calculator.sub(-1100, -100);
        Assert.assertEquals(actualResult, -1000);
    }

    @Test(groups = {"negative", "failTests"})
    public void testSub_OctalLiterals() {
        double actualResult = calculator.sub(050, 0010);
        Assert.assertEquals(actualResult, 40.0);
    }

    @Test(groups = "negative")
    public void testSub_WithExp() {
        double actualResult = calculator.sub(0.5e2, 0.1e2);
        Assert.assertEquals(actualResult, 40.0);
    }
}