package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class TestTg {
    Calculator calculator;
    private double angle;

    public TestTg(double angle) {
        this.angle = angle;
    }

    @Test(groups = {"positive", "failTests"})
    public void testTg(double angle) {
        double actualresult = calculator.tg(angle);
        Assert.assertEquals(actualresult, Math.sin(angle) / Math.cos(angle));
    }

    @Factory
    public Object[] factoryMethod() {
        return new Object[]{new TestTg(Math.PI / 3),
                            new TestTg(Math.PI / 4),
                            new TestTg(Math.PI / 6)
        };
    }

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = {"positive", "failTests"})
    public void testTg_Null() {
        double actualresult = calculator.tg(0);
        Assert.assertEquals(actualresult, 0);
    }

    @Test(groups = {"negative", "failTests"})
    public void testTg_AsymptoteValues() {
        double actualresult = calculator.tg(Math.PI / 2);
        Assert.assertEquals(actualresult, "Attempting to take tg() at the asymptote point");
    }
}