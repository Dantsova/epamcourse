package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestSin {
    Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = "positive")
    public void testSin_Null() {
        double actualResult = calculator.sin(0);
        Assert.assertEquals(actualResult, 0.0);
    }

    @Test(groups = "positive")
    public void testSin_SameValue() {
        double actualResult = calculator.sin(Math.PI / 2);
        Assert.assertEquals(actualResult, 1.0);
    }

    @Test(groups = "positive")
    public void testSin_SameValue2() {
        double actualResult = (double) Math.round(calculator.sin(Math.PI) * 10) / 10;
        Assert.assertEquals(actualResult, 0.0);
    }
}