package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestSqrt {
    Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = "positive")
    public void testSqrt_LongNumber() {
        double actualResult = calculator.sqrt(1);
        Assert.assertEquals(actualResult, 1.0);
    }

    @Test(groups = "positive")
    public void testSqrt_DoubleNumberWithPositiveResult() {
        double actualResult = calculator.sqrt(100.0);
        Assert.assertEquals(actualResult, 10.0);
    }

    @Test(groups = {"positive", "failTests"})
    public void testSqrt_DoubleNumber1WithNegativeResult() {
        double actualResult = calculator.sqrt(100.0);
        Assert.assertEquals(actualResult, -10.0);
    }

    @Test(groups = {"negative", "failTests"})
    public void testSqrt_Null() {
        double actualResult = calculator.sqrt(0);
        Assert.assertEquals(actualResult, "Attempt to take sqrt () from a negative number or null");
    }

    @Test(groups = {"negative", "failTests"})
    public void testSqrt_NegativeDoubleNumber() {
        double actualResult = calculator.sqrt(-100.0);
        Assert.assertEquals(actualResult, "Attempt to take sqrt() from a negative number or null");
    }
}