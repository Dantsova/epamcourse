package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestDiv {
    Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = "positive", priority = 0)
    public void testDiv_LongNumbers() {
        long actualResult = calculator.div(1000, 10);
        Assert.assertEquals(actualResult, 100);
    }

    @Test(groups = "positive", priority = 3)
    public void testDiv_DoubleNumbers() {
        double actualResult = calculator.div(1000.0, 10.0);
        Assert.assertEquals(actualResult, 100.0);
    }

    @Test(groups = "positive", priority = 2)
    public void testDiv_NegativeNumbers() {
        double actualResult = calculator.div(-1000.0, -10.0);
        Assert.assertEquals(actualResult, 100.0);
    }

    @Test(groups = "positive", priority = 1)
    public void testDiv_NegativeNumber() {
        double actualResult = calculator.div(1000.0, -10.0);
        Assert.assertEquals(actualResult, -100.0);
    }

    @Test(groups = "negative", expectedExceptions = NumberFormatException.class)
    public void testDiv_LongNull() {
        long actualResult = calculator.div(1000, 0);
    }

    @Test(expectedExceptions = NumberFormatException.class, groups = {"negative", "failTests"})
    public void testDiv_DoubleNull() {
        double actualResult = calculator.div(1000.0, 0.0);
    }
}