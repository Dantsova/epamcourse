package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestMult {
    Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = "positive")
    public void testMult_LongNumbers() {
        long actualResult = calculator.mult(12345679, 9);
        Assert.assertEquals(actualResult, 111111111);
    }

    @Test(groups = "positive")
    public void testMult_DoubleNumbers() {
        double actualResult = calculator.mult(5.00, 10.00);
        Assert.assertEquals(actualResult, 50.00);
    }

    @Test(groups = "positive", dependsOnMethods = "testMult_LongNumbers")
    public void testMult_WithNegativeNumbers() {
        long actualResult = calculator.mult(-10, -100);
        Assert.assertEquals(actualResult, 1000);
    }

    @Test(groups = {"negative", "failTests"})
    public void testMult_OctalLiterals() {
        double actualResult = calculator.sub(05, 0010);
        Assert.assertEquals(actualResult, 500.0);
    }

    @Test(groups = "negative")
    public void testMult_WithExp() {
        double actualResult = calculator.mult(0.5e2, 0.1e2);
        Assert.assertEquals(actualResult, 500.0);
    }
}