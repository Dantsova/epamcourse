package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestPow {
    Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = "positive")
    public void testPow_LongNumbers() {
        double actualResult = calculator.pow(10, 2);
        Assert.assertEquals(actualResult, 100.0);
    }

    @Test(groups = "positive")
    public void testPow_DoubleNumbers() {
        double actualResult = calculator.pow(10.0, 2.0);
        Assert.assertEquals(actualResult, 100.0);
    }

    @Test(groups = "positive")
    public void testPow_NegativeDegreeBasis() {
        double actualResult = calculator.pow(-10.0, 2);
        Assert.assertEquals(actualResult, 100.0);
    }

    @Test(groups = "positive")
    public void testPow_NegativeIndex() {
        double actualResult = calculator.pow(10.0, -2);
        Assert.assertEquals(actualResult, 0.01);
    }
}