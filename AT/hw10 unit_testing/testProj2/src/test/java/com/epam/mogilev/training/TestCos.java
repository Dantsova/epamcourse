package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestCos {
    Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(groups = {"positive", "failTests"})
    public void testCos_Null() {
        double actualResult = calculator.cos(0);
        Assert.assertEquals(actualResult, 1.0);
    }

    @Test(groups = {"positive", "failTests"})
    public void testCos_SameValue() {
        double actualResult = calculator.cos(Math.PI / 2);
        Assert.assertEquals(actualResult, 0.0);
    }

    @Test(groups = {"positive", "failTests"})
    public void testSin_SameValue2() {
        double actualResult = (double) Math.round(calculator.cos(Math.PI) * 10) / 10;
        Assert.assertEquals(actualResult, -1.0);
    }
}