package com.epam.mogilev.training;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class TestSum {

    private Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void testInitialize() {
        calculator = new Calculator();
    }

    @Test(dataProvider = "Data-Provider-Function", groups = "positive")
    public void testSum_LongNumbers(long number1, long number2) {
        long actualResult = calculator.sum(number1,number2);
        Assert.assertEquals(actualResult,number1+number2);
    }

    @DataProvider(name = "Data-Provider-Function")
    public Object[] parameterTestProvider() {
        Object numbers[][] = new Object[4][2];
        numbers[0][0] = 0;
        numbers[0][1] = 100;

        numbers[1][0] = -1;
        numbers[1][1] = 100;

        numbers[2][0] = 1;
        numbers[2][1] = -100;

        numbers[3][0] = -1;
        numbers[3][1] = -100;

        return numbers;
    }

    @Test(groups = {"positive", "failTests"})
    public void testSum_DoubleNumbers() {
        double actualResult = calculator.sum(0.09, 100.01);
        Assert.assertEquals(actualResult, 100.01);
    }

    @Test(groups = "positive")
    public void testSum_LongAndDoubleNumbers() {
        double actualResult = calculator.sum(1, 100.1);
        Assert.assertEquals(actualResult, 101.1);
    }

    @Test(groups = "positive")
    public void testSum_LongNegativeNumbers() {
        double actualResult = calculator.sum(-1, -100);
        Assert.assertEquals(actualResult, -101.0);
    }

    @Test(groups = {"negative", "failTests"})
    public void testSumOctalLiterals() {
        double actualResult = calculator.sum(05, 010);
        Assert.assertEquals(actualResult, 15.0);
    }

    @Test(groups = {"negative", "failTests"})
    public void testSum_HexaDecimalNumbers() {
        double actualResult = calculator.sum(0x5, 0x7);
        Assert.assertEquals(actualResult, 15.0);
    }

    @Test(groups = "negative")
    public void testSum_NumbersWithExp() {
        double actualResult = calculator.sum(1.2e2, 10);
        Assert.assertEquals(actualResult, 130.0);
    }
}