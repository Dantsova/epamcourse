package com.epam.at.hw14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SentPage extends Page {

    public SentPage(WebDriver driver) {
        super(driver);
    }

    private By locator_dataListLettersTo = By.xpath("//div[@class='b-datalist__item__panel']/div[@class='b-datalist__item__info']");
    private By locator_dataListSent_address = By.xpath("b-datalist__item__addr");
    private By locator_dataListSent_body = By.className("b-datalist__item__subj__snippet");

    public boolean findLetterInSent(String address, String mailBody) {
        boolean status = false;
        List <WebElement> elements = driver.findElements(locator_dataListLettersTo);
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).findElement(locator_dataListSent_address).getText().equals(address) &&
                    elements.get(i).findElement(locator_dataListSent_body).getText().equals(mailBody)) {
                elements.get(i).click();
                status = true;
                break;
            }
        }
        return status;
    }
}