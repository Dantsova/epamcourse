package com.epam.at.hw14.pages;

import com.epam.at.hw14.utils.Expectations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.UUID;

public class CreateNewLetterPage extends Page {
    private String subject = UUID.randomUUID().toString();

    private final By locator_inputAddress = By.cssSelector("textarea[data-original-name='To']");
    private final By locator_inputSubject = By.cssSelector("input[name='Subject']");
    private final By locator_inputMailBody = By.id("tinymce");
    private final By locator_btn_saveDraft = By.xpath("//div[@class='b-toolbar']//div[@data-name='saveDraft']");
    private final By locator_btn_sent = By.xpath("//div[@class='b-toolbar']//div[@data-name='send']");
    private final By locator_message_savedInDrafts = By.xpath("//div[@class='b-toolbar__item']//div[@data-mnemo='saveStatus']");
    private final By locator_message_sendLetter = By.xpath("//div[@class='message-sent__title']");

    public CreateNewLetterPage(WebDriver driver) {
        super(driver);
    }

    public CreateNewLetterPage enterMailAddress(String address) {
        Expectations.waitForElementVisibility(driver, locator_inputAddress);
        getDriver().findElement(locator_inputAddress).clear();
        getDriver().findElement(locator_inputAddress).sendKeys(address);
        return this;
    }

    public CreateNewLetterPage enterMailSubject() {
        Expectations.waitForElementVisibility(driver, locator_inputSubject);
        getDriver().findElement(locator_inputSubject).clear();
        driver.findElement(locator_inputSubject).sendKeys(subject);
        return this;
    }

    public CreateNewLetterPage enterMailBody(String mailBody) {
        WebElement iFrame = driver.findElement(By.tagName("iframe"));
        driver.switchTo().frame(iFrame);
        getDriver().findElement(locator_inputMailBody).clear();
        getDriver().findElement(locator_inputMailBody).sendKeys(mailBody);
        driver.switchTo().defaultContent();
        return this;
    }

    public CreateNewLetterPage clickOnSaveDraftButton() {
        getDriver().findElement(locator_btn_saveDraft).click();
        Expectations.waitForElementVisibility(driver, locator_message_savedInDrafts);
        return this;
    }

    public boolean isVisibleSaveStatus() {
        return elementIsPresent(locator_message_savedInDrafts);
    }

    public SentPage clickOnSendButton() {
        Expectations.waitForElementVisibility(driver, locator_btn_sent);
        getDriver().findElement(locator_btn_sent).click();
        return new SentPage(driver);
    }

    public boolean isVisibleSendStatus() {
        Expectations.waitForElementVisibility(driver, locator_message_sendLetter);
        return elementIsPresent(locator_message_sendLetter);
    }
}
