package com.epam.at.hw14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.epam.at.hw14.utils.Expectations.waitForElementClickability;
import static com.epam.at.hw14.utils.Expectations.waitForElementVisibility;

public class RegistrationPage extends Page {

    private static By locator_userMailName = By.xpath("//input[@id='mailbox:login']");
    private static By locator_userMailDomain = By.id("mailbox:domain");
    private static By locator_userMailPassword = By.id("mailbox:password");
    private static By locator_btn_logIn = By.id("mailbox:submit");

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    public RegistrationPage enterUserName(String login) {
        waitForElementVisibility(driver, locator_userMailName);
        driver.findElement(locator_userMailName).clear();
        driver.findElement(locator_userMailName).sendKeys(login);
        return this;
    }

    public RegistrationPage enterUserDomain(String domain) {
        driver.findElement(locator_userMailDomain);
        driver.findElement(locator_userMailDomain).sendKeys(domain);
        return this;
    }

    public RegistrationPage enterUserPassword(String password) {
        waitForElementVisibility(driver, locator_userMailPassword);
        driver.findElement(locator_userMailPassword).clear();
        driver.findElement(locator_userMailPassword).sendKeys(password);
        return this;
    }

    public RegistrationPage clickLoginButton() {
        waitForElementClickability(driver, locator_btn_logIn);
        driver.findElement(locator_btn_logIn).click();
        return new RegistrationPage(getDriver());
    }
}
