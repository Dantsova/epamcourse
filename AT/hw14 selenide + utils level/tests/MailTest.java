package com.epam.at.hw14.tests;

import com.epam.at.hw14.bisinessObject.User;
import com.epam.at.hw14.pages.*;
import com.epam.at.hw14.services.CreateLetterService;
import com.epam.at.hw14.services.Registration;
import com.epam.at.hw14.utils.Constants;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class MailTest extends ConfigurationTest {
    CreateLetterService creation = new CreateLetterService();
    Registration registration = new Registration();

    RegistrationPage registrationPage;
    CreateNewLetterPage createNewLetterPage;
    DraftPage draftPage;
    SentPage sentPage;
    MainPage mainPage;
    User user;

    @Before
    public void init() {
        user = new User(Constants.LOGIN, Constants.DOMAIN, Constants.PASSWORD);
        driver.get(Constants.URL);

        registrationPage = new RegistrationPage(driver);
        createNewLetterPage = new CreateNewLetterPage(driver);
        draftPage = new DraftPage(driver);
        mainPage = new MainPage(driver);
        sentPage = new SentPage(driver);
    }

    @Test
    public void logInTest() {
        mainPage = registration.entry(driver, user);

        Assert.assertEquals(mainPage.getUserMailBoxName(), user.getLogin() + user.getDomain());
        mainPage.clickOnLogOutLink();
    }

    @Test
    public void createDraftLetterTest() {
        mainPage = registration.entry(driver, user);

        createNewLetterPage = mainPage.clickOnWriteLetter();
        creation.createNewLetter(driver, Constants.ADDRESS, Constants.MAIL_BODY)
                .clickOnSaveDraftButton();

        draftPage = mainPage.clickOnDraftLink();
        Assert.assertTrue(draftPage.findLetterInDraft(Constants.ADDRESS, Constants.MAIL_BODY));
        mainPage.clickOnLogOutLink();
    }

    @Test
    public void sentDraftLetter_checkedSentExist_Test() {
        mainPage = registration.entry(driver, user);

        createNewLetterPage = mainPage.clickOnWriteLetter();
        creation.createNewLetter(driver, Constants.ADDRESS, Constants.MAIL_BODY)
                .clickOnSaveDraftButton();

        draftPage = mainPage.clickOnDraftLink();
        draftPage.findLetterInDraft(Constants.ADDRESS, Constants.MAIL_BODY);

        sentPage = createNewLetterPage.clickOnSendButton();

        Assert.assertTrue(sentPage.findLetterInSent(Constants.ADDRESS, Constants.MAIL_BODY));
        mainPage.clickOnLogOutLink();
    }

    @Test
    public void sentDraftLetter_checkedDraftNotExist_Test() {
        mainPage = registration.entry(driver, user);

        createNewLetterPage = mainPage.clickOnWriteLetter();
        creation.createNewLetter(driver, Constants.ADDRESS, Constants.MAIL_BODY)
                .clickOnSaveDraftButton();

        draftPage = mainPage.clickOnDraftLink();
        draftPage.findLetterInDraft(Constants.ADDRESS, Constants.MAIL_BODY);

        createNewLetterPage.clickOnSendButton();

        Assert.assertFalse(draftPage.findLetterInDraft(Constants.ADDRESS, Constants.MAIL_BODY));

        mainPage.clickOnLogOutLink();
    }

    @Test
    public void LogOutTest() {
        registration.entry(driver, user)
                .clickOnLogOutLink();

        Assert.assertEquals(mainPage.logOutSuccessful(), true);
    }
}