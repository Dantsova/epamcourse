package com.epam.at.hw14.services;

import com.epam.at.hw14.bisinessObject.User;
import com.epam.at.hw14.pages.MainPage;
import com.epam.at.hw14.pages.RegistrationPage;
import com.epam.at.hw14.utils.Expectations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Registration {

    public MainPage entry(WebDriver driver, User user) {
        new RegistrationPage(driver)
                .enterUserName(user.getLogin())
                .enterUserDomain(user.getDomain())
                .enterUserPassword(user.getPassword())
                .clickLoginButton();
        return new MainPage(driver);
    }
}
