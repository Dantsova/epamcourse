package com.epam.at.hw11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;

public class MailTest extends BaseTest {
    private static final String URL = "https://mail.ru/";
    private static final String ADRESS = "test@mail.ru";
    private String subject;
    private static final String EMAIL_BODY = "Anything important";

    private final By USER_MAIL_NAME = By.id("mailbox:login");
    private final By USER_MAIL_DOMAIN = By.id("mailbox:domain");
    private final By USER_MAIL_PASSWORD = By.id("mailbox:password");

    private final String expectedResult = "snegurka.belovegskaya@mail.ru";

    private final By userMail_locator = By.cssSelector("#PH_user-email");
    private final By btn_writeTheLatter_locator = By.cssSelector("#b-toolbar__left a[data-name='compose']");
    private final By txt_adress_locator = By.cssSelector("textarea[data-original-name='To']");
    private final By txt_subject_locator = By.cssSelector("input[name='Subject']");
    private final By txt_emailBody_locator = By.id("tinymce");
    private final By btn_saveDraft_locator = By.xpath("//div[@class='b-sticky']//div[@data-name='saveDraft']");
    private final By btn_draft_locator = By.cssSelector("a[data-mnemo='drafts']");

    public void authorization() {
        driver.get(URL);
        driver.findElement(USER_MAIL_NAME).sendKeys("snegurka.belovegskaya");
        driver.findElement(USER_MAIL_DOMAIN).sendKeys("@mail.ru");
        driver.findElement(USER_MAIL_PASSWORD).sendKeys("SNovimGodom!");
        driver.findElement(By.cssSelector("label[id='mailbox:submit']>input")).click();
    }

    @Test()
    public void testLogin() throws InterruptedException {
        authorization();
        waitForElementVisibility(userMail_locator);
        WebElement emailUser = driver.findElement(userMail_locator);

        Assert.assertEquals(emailUser.getText(), expectedResult);
    }

    @Test()
    public void testCreateLetterToDraft() throws InterruptedException {
        authorization();
        driver.findElement(btn_writeTheLatter_locator).click();

        driver.findElement(txt_adress_locator).sendKeys(ADRESS);

        subject = UUID.randomUUID().toString();
        driver.findElement(txt_subject_locator).sendKeys(subject);

        WebElement iFrame = driver.findElement(By.tagName("iframe"));
        driver.switchTo().frame(iFrame);
        WebElement textField = driver.findElement(txt_emailBody_locator);
        textField.sendKeys(EMAIL_BODY);
        driver.switchTo().defaultContent();

        driver.findElement(btn_saveDraft_locator).click();

        Thread.sleep(3000);
        //waitForElementVisibility(btn_draft_locator);
        driver.findElement(btn_draft_locator).click();

        waitForElementVisibility(By.cssSelector("a[data-subject='" + subject + "']"));
        driver.findElement(By.cssSelector("a[data-subject='" + subject + "']")).click();

        Assert.assertTrue(driver.getPageSource().contains(subject));

        Assert.assertTrue(driver.getPageSource().contains(ADRESS));

        Assert.assertTrue(driver.getPageSource().contains(EMAIL_BODY));
       }

    @Test()
    public void testSendTheMail() throws InterruptedException {
        authorization();
        driver.findElement(By.cssSelector("div[data-name='send']")).click();

        Thread.sleep(3000);
       // waitForElementVisibility(By.cssSelector("a[data-mnemo='drafts']"));
        driver.findElement(By.cssSelector("a[data-mnemo='drafts']")).click();

        Thread.sleep(3000);
        Assert.assertFalse(driver.getPageSource().contains(subject));

        driver.findElement(By.cssSelector("a[href=\"/messages/sent/\"]")).click();
        Thread.sleep(3000);

        Assert.assertTrue(driver.getPageSource().contains(subject));
    }

    @Test()
    public void testLogOff() {
        authorization();

        waitForElementVisibility(By.id("PH_logoutLink"));
        driver.findElement(By.id("PH_logoutLink")).click();

        Assert.assertEquals(driver.findElements(By.id("PH_authLink")).size(), 1);
    }

}
