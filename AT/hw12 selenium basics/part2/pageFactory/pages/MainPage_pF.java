package com.epam.at.hw12.part2.pageFactory.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage_pF extends Page_pF {
    private final By locator_login = By.id("PH_authLink");
    private final By locator_userBoxName = By.cssSelector("#PH_user-email");
    private final By locator_btn_writeTheLetter = By.cssSelector("#b-toolbar__left a[data-name='compose']");
    private final By locator_btn_logOut = By.cssSelector("#PH_logoutLink");

    @FindBy(id = "PH_user-email")
    private WebElement input_userBoxName;

    @FindBy(css = "#b-toolbar__left a[data-name='compose']")
    private WebElement btn_writeTheLetter;

    @FindBy(css = "#PH_logoutLink")
    private WebElement btn_logOut;

    public MainPage_pF(WebDriver driver) {
        super(driver);
    }

    public String getUserMailBoxName() {
        waitForElementVisibility(locator_userBoxName);
        return input_userBoxName.getText();
    }

    public CreateNewLettersPage_pF clickOnWriteTheLetter() {
        waitForElementVisibility(locator_btn_writeTheLetter);
        btn_writeTheLetter.click();
        return new CreateNewLettersPage_pF(driver);
    }

    public void clickOnLogOutButton() {
        waitForElementVisibility(locator_btn_logOut);
        btn_logOut.click();
    }

    public boolean getConfirmationPageWithButtonLogin() {
        waitForElementVisibility(locator_login);
        return elementIsPresent(locator_login);
    }
}