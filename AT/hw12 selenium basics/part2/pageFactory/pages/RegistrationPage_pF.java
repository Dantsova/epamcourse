package com.epam.at.hw12.part2.pageFactory.pages;

import com.epam.at.hw12.part2.pageObject.pages.RegistrationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage_pF extends Page_pF{

    @FindBy(id="mailbox:login")
    private WebElement input_logIn;

    @FindBy(id="mailbox:domain")
    private WebElement input_domain;

    @FindBy(id="mailbox:password")
    private WebElement input_password;

    @FindBy(css="label[id='mailbox:submit']>input")
    private WebElement btn_login;

    @FindBy(id="#PH_user-email")
    private  WebElement btn_userMailBoxName;

    @FindBy(id = "mailbox:error")
    private WebElement msg_registerError;

    private final By locator_userMailName = By.id("mailbox:login");

    public RegistrationPage_pF(WebDriver driver) {
        super(driver);
    }

    public RegistrationPage_pF enterUserName(String login){
        input_logIn.clear();
        input_logIn.sendKeys(login);
        return new RegistrationPage_pF(getDriver());
    }

    public RegistrationPage_pF enterUserDomain(String domain){
        input_domain.sendKeys(domain);
        return new RegistrationPage_pF(getDriver());
    }

    public RegistrationPage_pF enterUserPassword(String password){
        input_password.clear();
        input_password.sendKeys(password);
        return new RegistrationPage_pF(getDriver());
    }

    public MainPage_pF registration(String login, String domain, String password){
        RegistrationPage_pF registrationPage = new RegistrationPage_pF(driver);
        registrationPage.enterUserName(login)
                        .enterUserDomain(domain)
                        .enterUserPassword(password)
                        .pressLoginButton();
        return new MainPage_pF(getDriver());
    }

    public MainPage_pF pressLoginButton() {
        btn_login.click();
        return new MainPage_pF(getDriver());
    }
}
