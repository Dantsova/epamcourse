package com.epam.at.hw12.part2.pageFactory.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.UUID;

public class CreateNewLettersPage_pF extends Page_pF {

    private String subject;
    private final By locator_messege_savedInDrafts = By.className("b-toolbar__message");

    @FindBy(css="textarea[data-original-name='To']")
    private WebElement input_adress;

    @FindBy(css="input[name='Subject']")
    private WebElement input_subject;

    @FindBy(css="#tinymce")
    private WebElement input_mailBody;

    @FindBy(xpath="//div[@class='b-sticky']//div[@data-name='saveDraft']")
    private WebElement btn_saveDraft;

    public CreateNewLettersPage_pF(WebDriver driver) {
        super(driver);
    }

    public CreateNewLettersPage_pF enterMailAdress(String adress) {
        input_adress.clear();
        input_adress.sendKeys(adress);
        return new CreateNewLettersPage_pF(getDriver());
    }

    public CreateNewLettersPage_pF enterMailSubject() {
        input_subject.clear();
        subject = UUID.randomUUID().toString();
        input_subject.sendKeys(subject);
        return new CreateNewLettersPage_pF(getDriver());
    }

    public CreateNewLettersPage_pF enterMailBody(String mailBody){
        WebElement iFrame = driver.findElement(By.tagName("iframe"));
        driver.switchTo().frame(iFrame);
        input_mailBody.clear();
        input_mailBody.sendKeys(mailBody);
        driver.switchTo().defaultContent();
        return new CreateNewLettersPage_pF(getDriver());
    }

    public CreateNewLettersPage_pF createNewLetter(String adress, String mailBody){
        CreateNewLettersPage_pF createNewLetterPage = new CreateNewLettersPage_pF(getDriver());
        createNewLetterPage.enterMailAdress(adress)
                           .enterMailSubject()
                           .enterMailBody(mailBody);
        return new CreateNewLettersPage_pF(getDriver());
    }

    public CreateNewLettersPage_pF clickOnSaveDraft() {
        btn_saveDraft.click();
        return new CreateNewLettersPage_pF(getDriver());
    }

    public boolean getSaveStatus(){
        return elementIsPresent(locator_messege_savedInDrafts);
    }
}