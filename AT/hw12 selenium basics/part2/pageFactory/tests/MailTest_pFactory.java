package com.epam.at.hw12.part2.pageFactory.tests;

import com.epam.at.hw12.part2.pageFactory.pages.CreateNewLettersPage_pF;
import com.epam.at.hw12.part2.pageFactory.pages.MainPage_pF;
import com.epam.at.hw12.part2.pageFactory.pages.RegistrationPage_pF;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MailTest_pFactory extends ConfigurationTest_pF {
    private static final String URL = "https://www.mail.ru/";

    private static  final String LOGIN = "snegurka.belovegskaya";
    private static  final String DOMAIN  = "@mail.ru";
    private static  final String PASSWORD = "SNovimGodom!";

    private static final String ADRESS = "test@mail.ru";
    private static final String MAIL_BODY = "Anything important";

    @Test
    public void loginTest(){
        driver.get(URL);
        RegistrationPage_pF registrationPage = new RegistrationPage_pF(driver);
        MainPage_pF mainPage = registrationPage.registration(LOGIN,DOMAIN, PASSWORD);

        Assert.assertEquals(mainPage.getUserMailBoxName(), LOGIN + DOMAIN);
    }

    @Test
    public void createDraftLetterTest(){
        driver.get(URL);
        RegistrationPage_pF registrationPage = new RegistrationPage_pF(driver);
        MainPage_pF mainPage = registrationPage.registration(LOGIN,DOMAIN,PASSWORD);

        CreateNewLettersPage_pF createNewLetterPage = mainPage.clickOnWriteTheLetter();
        createNewLetterPage.enterMailAdress(ADRESS)
                            .enterMailSubject()
                            .enterMailBody(MAIL_BODY)
                            .clickOnSaveDraft();

        Assert.assertEquals(createNewLetterPage.getSaveStatus(),true );
    }

    @Test
    public void clickOnLogOutButton_getButtonLogin_Test(){
        driver.get(URL);
        RegistrationPage_pF registrationPage = new RegistrationPage_pF(driver);
        MainPage_pF mainPage = registrationPage.registration(LOGIN,DOMAIN, PASSWORD);

        mainPage.clickOnLogOutButton();

        Assert.assertEquals(mainPage.getConfirmationPageWithButtonLogin(),true);
    }
}