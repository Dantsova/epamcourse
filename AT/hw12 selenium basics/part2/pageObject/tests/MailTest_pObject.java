package epam.at.hw14.tests;

import epam.at.hw14.pages.CreateNewLetterPage;
import epam.at.hw14.pages.DraftPage;
import epam.at.hw14.pages.MainPage;
import epam.at.hw14.pages.RegistrationPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MailTest_pObject extends ConfigurationTest {
    private static final String URL = "https://www.mail.ru/";
    private static final String INFO = "Selenium";

    private static final String LOGIN = "snegurka.belovegskaya";
    private static final String DOMAIN = "@mail.ru";
    private static final String PASSWORD = "SNovimGodom!";

    private static final String ADRESS = "test@mail.ru";
    private String subject;
    private static final String MAIL_BODY = "Anything important";

    @Test
    public void logInTest() {
        driver.get(URL);
        RegistrationPage registrationPage = new RegistrationPage(driver);
        MainPage mainPage = registrationPage.registration(LOGIN, DOMAIN, PASSWORD);

        Assert.assertEquals(mainPage.getUserMailBoxName(), LOGIN + DOMAIN);
    }

    @Test
    public void createDraftLetterTest() {
        driver.get(URL);
        RegistrationPage registrationPage = new RegistrationPage(driver);
        MainPage mainPage = registrationPage.registration(LOGIN, DOMAIN, PASSWORD);

        CreateNewLetterPage createNewLetterPage = mainPage.clickOnWriteLetter();
        createNewLetterPage.createNewLetter(ADRESS, MAIL_BODY)
                .clickOnSaveDraftButton();

        Assert.assertEquals(createNewLetterPage.getSaveStatus(), true);
    }

    @Test
    public void sendDraftLetterTest() throws InterruptedException {
        driver.get(URL);
        RegistrationPage registrationPage = new RegistrationPage(driver);
        MainPage mainPage = registrationPage.registration(LOGIN, DOMAIN, PASSWORD);

        CreateNewLetterPage createNewLetterPage = mainPage.clickOnWriteLetter();
        createNewLetterPage.createNewLetter(ADRESS, MAIL_BODY)
                .clickOnSaveDraftButton();

        DraftPage draftPage = mainPage.clickOnDraftLink();

        createNewLetterPage = draftPage
                .findLetter(MAIL_BODY)
                .clickOnSendButton();

        Assert.assertEquals(createNewLetterPage.getSendStatus(), true);
    }

    @Test
    public void LogOutTest() {
        driver.get(URL);
        RegistrationPage registrationPage = new RegistrationPage(driver);
        MainPage mainPage = registrationPage.registration(LOGIN, DOMAIN, PASSWORD);

        mainPage.clickOnLogOutLink();

        Assert.assertEquals(mainPage.getConfirmationPageWithButtonLogin(), true);
    }
}