package epam.at.hw14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DraftPage extends Page {

    private final By locator_dataList_draft = By.cssSelector("span[class='b-datalist__item__subj__snippet']");
    private final By locator_btn_remove = By.xpath("//div[@class='b-toolbar__item']//div[@data-name='remove']");
    private final By lacator_draft_label_active = By.xpath("//div[@data-id='500001' and @class='b-nav__item js-href b-nav__item_active']");

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public CreateNewLetterPage findLetter(String mailBody) {
        waitForElementVisibility(lacator_draft_label_active);
        List <WebElement> elements = driver.findElements(locator_dataList_draft);
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).getAttribute("textContent").contains(mailBody)) {
                elements.get(i).click();
                break;
            }
        }
        return new CreateNewLetterPage(getDriver());
    }
}
