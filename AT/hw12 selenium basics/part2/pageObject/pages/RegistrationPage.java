package epam.at.hw14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage extends Page {
    private final By locator_userMailName = By.id("mailbox:login");
    private final By locator_userMailDomain = By.cssSelector("select[id='mailbox:domain']");
    private final By locator_userMailPassword = By.id("mailbox:password");

    private final By locator_btn_logIn = By.cssSelector("label[id='mailbox:submit']>input");

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    public RegistrationPage enterUserName(String login) {
        waitForElementVisibility(locator_userMailName);
        getDriver().findElement(locator_userMailName).clear();
        getDriver().findElement(locator_userMailName).sendKeys(login);
        return new RegistrationPage(driver);
    }

    public RegistrationPage enterUserDomain(String domain) {
        getDriver().findElement(locator_userMailDomain);
        getDriver().findElement(locator_userMailDomain).sendKeys(domain);
        return new RegistrationPage(driver);
    }

    public RegistrationPage enterUserPassword(String password) {
        waitForElementVisibility(locator_userMailPassword);
        getDriver().findElement(locator_userMailPassword).clear();
        getDriver().findElement(locator_userMailPassword).sendKeys(password);
        return new RegistrationPage(driver);
    }

    public MainPage clickLoginButton() {
        getDriver().findElement(locator_btn_logIn).click();
        return new MainPage(getDriver());
    }

    public MainPage registration(String login, String domain, String password) {
        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.enterUserName(login)
                .enterUserDomain(domain)
                .enterUserPassword(password)
                .clickLoginButton();
        return new MainPage(getDriver());
    }
}
