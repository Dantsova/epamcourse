package epam.at.hw14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends Page {
    private final By locator_login = By.id("PH_authLink");
    private final By locator_userBoxName = By.cssSelector("#PH_user-email");
    private final By locator_btn_writeTheLetter = By.cssSelector("#b-toolbar__left a[data-name='compose']");
    private final By locator_btn_remove = By.xpath("//div[@class='b-toolbar__item']//div[@data-name='remove']");
    private final By locator_link_sent = By.xpath("//a[@class='b-nav__link js-shortcut']//span[@class='b-nav__item__text']");
    private final By locator_link_draft = By.cssSelector("a[data-mnemo='send']>span");
    private final By locator_link_logOut = By.cssSelector("#PH_logoutLink");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public String getUserMailBoxName() {
        waitForElementVisibility(locator_userBoxName);
        return driver.findElement(locator_userBoxName).getText();
    }

    public CreateNewLetterPage clickOnWriteLetter() {
        waitForElementVisibility(locator_btn_writeTheLetter);
        driver.findElement(locator_btn_writeTheLetter).click();
        return new CreateNewLetterPage(driver);
    }

    public DraftPage clickOnDraftLink() {
        waitForElementVisibility(locator_link_draft);
        driver.findElement(locator_link_draft).click();
        return new DraftPage(getDriver());
    }

    public SentPage clickOnSentLink() {
        waitForElementVisibility(locator_link_sent);
        driver.findElement(locator_link_sent).click();
        return new SentPage(getDriver());
    }

    public void clickOnLogOutLink() {
        waitForElementVisibility(locator_link_logOut);
        driver.findElement(locator_link_logOut).click();
    }

    public boolean getConfirmationPageWithButtonLogin() {
        waitForElementVisibility(locator_login);
        return elementIsPresent(locator_login);
    }
}