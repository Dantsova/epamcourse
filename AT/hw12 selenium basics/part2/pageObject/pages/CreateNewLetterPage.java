package epam.at.hw14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.UUID;

public class CreateNewLetterPage extends Page {
    private String subject;

    private final By locator_inputAdress = By.cssSelector("textarea[data-original-name='To']");
    private final By locator_inputSubject = By.cssSelector("input[name='Subject']");
    private final By locator_inputEmailBody = By.id("tinymce");
    private final By locator_btn_saveDraft = By.xpath("//div[@class='b-toolbar']//div[@data-name='saveDraft']");
    private final By locator_btn_send = By.xpath("//div[@class='b-sticky']//div[@data-name='send']");
    private final By locator_messege_savedInDrafts = By.xpath("//div[@class='b-toolbar__item']//div[@data-mnemo='saveStatus']");
    private final By locator_messege_sendLetter = By.xpath("//div[@class='message-sent__title']");

    public CreateNewLetterPage(WebDriver driver) {
        super(driver);
    }

    public CreateNewLetterPage enterMailAdress(String adress) {
        waitForElementVisibility(locator_inputAdress);
        getDriver().findElement(locator_inputAdress).clear();
        getDriver().findElement(locator_inputAdress).sendKeys(adress);
        return this;
    }

    public CreateNewLetterPage enterMailSubject() {
        waitForElementVisibility(locator_inputSubject);
        getDriver().findElement(locator_inputSubject).clear();
        subject = UUID.randomUUID().toString();
        driver.findElement(locator_inputSubject).sendKeys(subject);
        return this;
    }

    public CreateNewLetterPage enterMailBody(String mailBody) {
        WebElement iFrame = driver.findElement(By.tagName("iframe"));
        driver.switchTo().frame(iFrame);
        getDriver().findElement(locator_inputEmailBody).clear();
        getDriver().findElement(locator_inputEmailBody).sendKeys(mailBody);
        driver.switchTo().defaultContent();
        return this;
    }

    public CreateNewLetterPage createNewLetter(String adress, String mailBody) {
        CreateNewLetterPage createNewLetterPage = new CreateNewLetterPage(getDriver());
        createNewLetterPage.enterMailAdress(adress)
                .enterMailSubject()
                .enterMailBody(mailBody);
        return this;
    }

    public CreateNewLetterPage clickOnSaveDraftButton() {
        getDriver().findElement(locator_btn_saveDraft).click();
        waitForElementVisibility(locator_messege_savedInDrafts);
        return this;
    }

    public boolean getSaveStatus() {
        return elementIsPresent(locator_messege_savedInDrafts);
    }

    public CreateNewLetterPage clickOnSendButton() {
        waitForElementVisibility(locator_btn_send);
        getDriver().findElement(locator_btn_send).click();
        return this;
    }

    public boolean getSendStatus() {
        waitForElementVisibility(locator_messege_sendLetter);
        return elementIsPresent(locator_messege_sendLetter);
    }

}
