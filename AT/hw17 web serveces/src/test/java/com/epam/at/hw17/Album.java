package com.epam.at.hw17;

public class Album {
    Integer id;
    String title;
    Integer userId;

    public Album(Integer id, String title, Integer userId) {
        this.id = id;
        this.title = title;
        this.userId = userId;
    }

    public Album() {
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Integer getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "{\n" +
                "userId: " + getUserId() + "\n" +
                "id: " + getId() + "\n" +
                "title: " + getTitle() + "\n" +
                "}";
    }
}
