package com.epam.at.hw17;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.core.Is.is;

public class RestAssuredTask {

    @BeforeClass
    public void before() {
        RestAssured.baseURI = "http://jsonplaceholder.typicode.com";
    }

    @Test
    public void verifyStatusCode() {
        given().
                when().get("/albums").
                then().statusCode(200).
                extract().response().print();
    }

    @Test
    public void verifyHeader() {
        Response response = given().
                when().get("/albums");
        Assert.assertTrue(response.getHeaders().hasHeaderWithName("content-type"));
        Assert.assertEquals(response.getHeader("content-type"), "application/json; charset=utf-8");
    }

    @Test
    public void verifyBody() {
        given().
                when().get("/albums").
                then().statusCode(200).assertThat().body("size()", is(100));
    }

    @Test
    public void verifyInfo() {
        given().
                when().get("/albums/7").
                then().assertThat().statusCode(200).body("$", hasKey("userId")).body("$", hasKey("id")).body("$", hasKey("title"));
    }

    @Test
    public void updateAlbum() {
        int id = given().body(new Album(1, "test album", 1)).contentType(ContentType.JSON).
                when().post("/albums").
                then().assertThat().statusCode(201).body("isEmpty()", Matchers.is(false)).
                extract().path("id");
        System.out.println(id);
    }

    @Test
    public void updatePost() {
        Album updatedAlbum = new Album(1, "test album update", 1);
        given().
                pathParam("id", updatedAlbum.getId()).
                body(updatedAlbum.toString()).
                when().put("/albums/{id}").
                then().assertThat().statusCode(200).body("id", is(updatedAlbum.getId())).extract().response().print();
    }

    @Test()
    public void DeleteElement() {
        given().
                when().delete("/albums/1").
                then().assertThat().statusCode(200);
    }
}