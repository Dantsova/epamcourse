package com.epam.at.hw17;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RestTemplateTask {

    private static String baseUrl = "http://jsonplaceholder.typicode.com";

    @Test
    public void verifyStatusCode() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity <String> response = restTemplate.exchange(baseUrl + "/albums", HttpMethod.GET, null, String.class);
        Assert.assertEquals(response.getStatusCode().value(), 200);
    }
}