package com.epam.at.hw18.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class HomePage extends AbstractPage {
    private Logger logger = LogManager.getLogger(this.getClass());
    private static final String BASE_URL = "https://www.ebay.com/";
    private final By submit = By.xpath("//input[@id='gh-btn']");

    public HomePage open() {
        driver.get(BASE_URL);
        logger.warn("Opening " + BASE_URL);
        return this;
    }

    public SearchPage clickSubmit() {
        logger.info("Waiting until button " + submit + " would not be visible");
        waitForElementVisible(submit);
        driver.findElement(submit).click();
        logger.warn("Button Submit was pressed");
        return new SearchPage();
    }
}