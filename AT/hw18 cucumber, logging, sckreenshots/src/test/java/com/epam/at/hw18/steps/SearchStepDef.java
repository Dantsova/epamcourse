package com.epam.at.hw18.steps;

import com.epam.at.hw18.browser.Browser;
import com.epam.at.hw18.pages.HomePage;
import com.epam.at.hw18.pages.SearchPage;
import com.epam.at.hw18.services.EnterQueryServices;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class SearchStepDef extends Browser {
    private Logger logger = LogManager.getLogger(this.getClass());
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 'at' HH.mm.ss");

    @Given("^I opened Ebay.com page$")
    public void iOpenedSitePage() throws IOException {
        HomePage homePage = new HomePage().open();
        File screenshot = ((TakesScreenshot) homePage.getWebdriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("./test-output/screenshots/" + dateFormat.format(new Date()) + ".png"));
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) throws IOException {
        logger.info("Searching the product");
        EnterQueryServices enterQueryServices = new EnterQueryServices().enterQuery(query);
        File screenshot = ((TakesScreenshot) enterQueryServices.getWebdriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("./test-output/screenshots/" + dateFormat.format(new Date()) + ".png"));
        new HomePage().clickSubmit();
    }

    @Then("^The term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) throws IOException {
        SearchPage searchPage = new SearchPage();
        File screenshot = ((TakesScreenshot) searchPage.getWebdriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("./test-output/screenshots/" + dateFormat.format(new Date()) + ".png"));
        assertThat(searchPage.getTextFromFirstProduct().toLowerCase(), containsString(expectPhrase.toLowerCase()));
    }
}
