package com.epam.at.hw18.services;

import com.epam.at.hw18.pages.AbstractPage;
import org.openqa.selenium.By;

public class EnterQueryServices extends AbstractPage {

    private final By searchInput = By.xpath("//div[@id='gh-ac-box']//input");

    public EnterQueryServices enterQuery(String query) {
        waitForElementVisible(searchInput);
        driver.findElement(searchInput).sendKeys(query);
        return this;
    }

}
