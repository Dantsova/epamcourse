package com.epam.at.hw18.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

public class TestListener extends RunListener {
    private Logger logger = LogManager.getLogger("Test Listener");

    @Override
    public void testStarted(Description description) {
        logger.info("Test was started " + description.getDisplayName());
    }

    @Override
    public void testFinished(Description description) {
        logger.info("Test was finished " + description.getDisplayName());
    }

    @Override
    public void testFailure(Failure failure) {
        logger.error("Test was failed " + failure.getMessage());
    }
}
