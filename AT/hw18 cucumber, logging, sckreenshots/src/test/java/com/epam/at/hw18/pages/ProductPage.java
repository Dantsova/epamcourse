package com.epam.at.hw18.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class ProductPage extends AbstractPage {
    private Logger logger = LogManager.getLogger(this.getClass());
    private final By buttonAddToCart = By.id("isCartBtn_btn");
    private final By linkTitleProduct = By.xpath("//*[@class='s-item__info clearfix']//a/h3");

    public String getTextFromNameProduct() {
        logger.info("Waiting until Title of product " + linkTitleProduct + " would not be visible");
        waitForElementVisible(linkTitleProduct);
        return driver.findElement(linkTitleProduct).getText();
    }

    public CartPage clickOnAddtoCardButton() {
        logger.info("Waiting until button " + buttonAddToCart + " would not be visible");
        waitForElementVisible(buttonAddToCart);
        driver.findElement(buttonAddToCart).click();
        logger.warn("Button Submit was pressed");
        return new CartPage();
    }
}