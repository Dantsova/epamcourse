package com.epam.at.hw18.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

public class CartPage extends AbstractPage {
    private final By listOfProducts = By.xpath("//span[@class='BOLD']");
    private Logger logger = LogManager.getLogger(getClass());

    public String getTextFromFirstProduct() {
        String nameOfProduct = driver.findElements(listOfProducts).get(0).getText();
        logger.info("Reading the list of products in the Cart and getting the name of the first one");
        return nameOfProduct;
    }
}
