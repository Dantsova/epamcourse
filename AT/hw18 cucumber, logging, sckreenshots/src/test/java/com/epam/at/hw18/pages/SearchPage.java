package com.epam.at.hw18.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchPage extends AbstractPage {
    private Logger logger = LogManager.getLogger(this.getClass());
    private final By listOfProducts = By.xpath("//*[@id='ListViewInner']//h3/a");

    public ProductPage clickOnFirstProduct() {
        logger.info("Waiting until list of products " + listOfProducts + " would not be visible");
        waitForElementVisible(listOfProducts);
        List <WebElement> elements = driver.findElements(listOfProducts);
        elements.get(0).click();
        logger.warn("Link first element of Search list was pressed");
        return new ProductPage();
    }

    public String getTextFromFirstProduct() {
        logger.info("Waiting until list of products " + listOfProducts + " would not be visible");
        waitForElementVisible(listOfProducts);
        List <WebElement> elements = driver.findElements(listOfProducts);
        String q = elements.get(0).getAttribute("data-mtdes");
        logger.warn("Title first element of Search list was getting");
        return q;
    }
}
