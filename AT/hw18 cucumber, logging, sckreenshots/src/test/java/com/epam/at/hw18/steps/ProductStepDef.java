package com.epam.at.hw18.steps;

import com.epam.at.hw18.browser.Browser;
import com.epam.at.hw18.pages.CartPage;
import com.epam.at.hw18.pages.SearchPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class ProductStepDef extends Browser {

    @When("^I add the product \"([^\"]*)\" to cart$")
    public void iAddProductToCart(String expectTitle) throws IOException {
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnFirstProduct().clickOnAddtoCardButton();
        File screenshot = ((TakesScreenshot) searchPage.getWebdriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("./test-output/screenshots/" + screenshot.getName()));
    }

    @Then("^The term \"([^\"]*)\" should be the first in the Сart in the list of goods$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectTitle) throws IOException {
        CartPage cartPage = new CartPage();
        File screenshot = ((TakesScreenshot) cartPage.getWebdriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("./test-output/screenshots/" + screenshot.getName()));
        assertThat(cartPage.getTextFromFirstProduct().toLowerCase(), containsString(expectTitle.toLowerCase()));
    }
}

