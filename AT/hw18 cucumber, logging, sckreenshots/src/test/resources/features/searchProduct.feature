@great
Feature: Search product

  Scenario: Running a Full Text Quick Search
    Given I opened Ebay.com page
    When I search the product "table"
    Then The term query "table" should be the first in the Search Result grid

