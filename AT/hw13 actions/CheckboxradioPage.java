package com.epam.at.hw13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class CheckboxradioPage extends Page {
    private By newyork = By.xpath("//label[@for='radio-1']");
    private By twoStar = By.xpath("//label[@for='checkbox-1']");

    public CheckboxradioPage(WebDriver driver) {
        super(driver);
    }

    public boolean isSelectedNewYork() {
        return driver.findElement(newyork).getAttribute("class").contains("ui-state-active");
    }

    public boolean isSelectedTwoStar() {
        return driver.findElement(twoStar).getAttribute("class").contains("ui-state-active");
    }

    public void selectNewYork() {
        switchToFrame();
        waitForElementClickable(newyork);
        if (!isSelectedNewYork()) {
            Actions actions = new Actions(driver);
            actions.click(driver.findElement(newyork)).build().perform();
        }
        driver.switchTo().parentFrame();
    }

    public void selectTwoStar() {
        switchToFrame();
        waitForElementClickable(twoStar);
        if (!isSelectedTwoStar()) {
            Actions actions = new Actions(driver);
            actions.click(driver.findElement(twoStar)).build().perform();
        }
        driver.switchTo().parentFrame();
    }
}