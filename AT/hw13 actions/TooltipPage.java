package com.epam.at.hw13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class TooltipPage extends Page {
    private By tooltip_Link = By.xpath("//a[text()='Tooltips']");
    private By toolTip_locator = By.xpath("//div[@class='ui-tooltip-content']");
    private String tooltip_text = "That's what this widget is";

            public TooltipPage(WebDriver driver) {
                super(driver);
            }

            public void hoverMouseTooltips() {
                switchToFrame();
                Actions actionsTooltips = new Actions(driver);
                actionsTooltips.moveToElement(driver.findElement(tooltip_Link)).build().perform();
    }

            public boolean isShowTooltips() {
                return driver.findElement(toolTip_locator).getText().equals(tooltip_text);
            }
}