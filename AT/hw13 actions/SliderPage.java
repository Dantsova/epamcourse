package com.epam.at.hw13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class SliderPage extends Page {
    private By slider = By.xpath("//span[@class='ui-slider-handle ui-corner-all ui-state-default']");
    private By targetSlider = By.xpath("//span[@class='ui-slider-handle ui-corner-all ui-state-default ui-state-focus ui-state-hover']");
    protected String textSlider = "left: 0%;";

    public SliderPage(WebDriver driver) {
        super(driver);
    }

    public void dragElement() {
        switchToFrame();
        Actions actions = new Actions(driver);
        actions.dragAndDropBy(driver.findElement(slider), 100, 10).build().perform();
        driver.switchTo().parentFrame();
    }

    public boolean isMoveDragElement() {
        return !driver.findElement(targetSlider).getAttribute("style").equals(textSlider);
    }
}