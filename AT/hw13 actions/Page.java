package com.epam.at.hw13;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    private final By droppableLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'droppable')]");
    private final By checkboxradioLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'checkboxradio')]");
    private final By selectmenuLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'selectmenu')]");
    private final By tooltipLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'tooltip')]");
    private final By sliderLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'slider')]");

    private final By frame = By.cssSelector(".demo-frame");
    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void clickDroppableLink() {
        waitForElementClickable(droppableLink);
        click(driver.findElement(droppableLink));
    }

    public void clickCheckboxradioLink() {
        waitForElementClickable(checkboxradioLink);
        click(driver.findElement(checkboxradioLink));
    }

    public void clickSelectmenuLink() {
        waitForElementClickable(selectmenuLink);
        click(driver.findElement(selectmenuLink));
    }

    public void clickTooltipLink() {
        waitForElementClickable(tooltipLink);
        click(driver.findElement(tooltipLink));
    }

    public void clickSliderLink() {
        waitForElementClickable(sliderLink);
        click(driver.findElement(sliderLink));
    }

    public void switchToFrame() {
        driver.switchTo().frame(driver.findElement(frame));
    }

    public void switchToDefaultContent() {
        driver.switchTo().defaultContent();
    }

    public void waitForElementClickable(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void click(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }
}

