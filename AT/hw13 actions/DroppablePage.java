package com.epam.at.hw13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DroppablePage extends Page {
    private By dropElement = By.id("draggable");
    private By targetElement = By.id("droppable");
    protected String textTargetElement = "Dropped!";

    public DroppablePage(WebDriver driver) {
        super(driver);
    }

    public void dropElement() {
        switchToFrame();
        WebElement element = driver.findElement(dropElement);
        Actions actions = new Actions(driver);
        WebElement element2 = driver.findElement(targetElement);
        actions.dragAndDrop(element, element2).build().perform();
        driver.switchTo().parentFrame();
    }

    public boolean isDropElement() {
       return driver.findElement(targetElement).getText().contains(textTargetElement);
    }
}
