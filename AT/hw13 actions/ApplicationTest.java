package com.epam.at.hw13;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ApplicationTest extends BaseTest {

    @Test
    public void droppablePageDropTest() {
        DroppablePage droppablePage = new DroppablePage(driver);
        droppablePage.clickDroppableLink();
        droppablePage.dropElement();
        Assert.assertTrue(droppablePage.isDropElement());
    }

    @Test
    public void checkboxradioPageCheckbowNewYorkTest() {
        CheckboxradioPage checkboxradioPage = new CheckboxradioPage(driver);
        checkboxradioPage.clickCheckboxradioLink();
        Assert.assertTrue(checkboxradioPage.isSelectedNewYork());
    }

    @Test
    public void checkboxradioPageCheckboxTwoStarTest() {
        CheckboxradioPage checkboxradioPage = new CheckboxradioPage(driver);
        checkboxradioPage.clickCheckboxradioLink();
        Assert.assertTrue(checkboxradioPage.isSelectedTwoStar());
    }

    @Test
    public void selectmenuPageSpeedTest() {
        SelectmenuPage selectmenuPage = new SelectmenuPage(driver);
        selectmenuPage.clickSelectmenuLink();
        selectmenuPage.selectSpeed();
        Assert.assertTrue(selectmenuPage.isSelectSpeed());
    }

    @Test
    public void selectmenuPageNumberTest() {
        SelectmenuPage selectmenuPage = new SelectmenuPage(driver);
        selectmenuPage.clickSelectmenuLink();
        selectmenuPage.selectNumber();
        Assert.assertTrue(selectmenuPage.isSelectNumber());
    }

    @Test
    public void tooltipPageTooltipsTest() {
        TooltipPage tooltipPage = new TooltipPage(driver);
        tooltipPage.clickTooltipLink();
        tooltipPage.hoverMouseTooltips();
        Assert.assertTrue(tooltipPage.isShowTooltips());
    }

    @Test
    public void sliderPageDragTest() {
        SliderPage sliderPage = new SliderPage(driver);
        sliderPage.clickSliderLink();
        sliderPage.dragElement();
        Assert.assertTrue(sliderPage.isMoveDragElement());
    }
}