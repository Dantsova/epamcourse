package com.epam.at.hw13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class SelectmenuPage extends Page {
    private By selectSpeed = By.id("speed-button");
    private By speedFast = By.xpath("//div[text()='Fast']");
    private By selectMenu_Fast = By.xpath("//span[text()='Fast']");
    private By selectNumber = By.id("number-button");
    private By numberThree = By.xpath("//div[text()='3']");
    private By selectMenu_3 = By.xpath("//span[text()='3']");
    private String speedFast_text = "Fast";
    private String numberTree_text = "3";

    public SelectmenuPage(WebDriver driver) {
        super(driver);
    }

    public void selectSpeed() {
        switchToFrame();
        Actions actionsSelect = new Actions(driver);
        actionsSelect.click(driver.findElement(selectSpeed)).build().perform();
        actionsSelect.click(driver.findElement(speedFast)).build().perform();
        driver.switchTo().parentFrame();
        //   Select drpSelectSpeed = new Select(driver.findElement(By.name("speed")));
        //   drpSelectSpeed.selectByVisibleText("Fast");
    }

    public boolean isSelectSpeed() {
         return driver.findElement(selectMenu_Fast).getText().equals(speedFast_text);
    }

    public void selectNumber() {
        switchToFrame();
        click(driver.findElement(selectNumber));
        Actions actionsSelected = new Actions(driver);
        actionsSelected.click(driver.findElement(numberThree)).build().perform();
    }

    public boolean isSelectNumber() {
         return driver.findElement(selectMenu_3).getText().equals(numberTree_text);
    }
}
