package com.epam.at.hw15.services;

import com.epam.at.hw15.pages.CreateNewLetterPage;
import org.openqa.selenium.WebDriver;

public class Creation {
    CreateNewLetterPage createNewLetterPage;

    public CreateNewLetterPage createNewLetter(WebDriver driver, String adress, String mailBody) {
        createNewLetterPage = new CreateNewLetterPage();
        createNewLetterPage.enterMailAddress(adress)
                .enterMailSubject()
                .enterMailBody(mailBody);
        return new CreateNewLetterPage();
    }
}