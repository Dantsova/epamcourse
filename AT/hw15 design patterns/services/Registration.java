package com.epam.at.hw15.services;

import com.epam.at.hw15.bisinessObject.User;
import com.epam.at.hw15.pages.MainPage;
import com.epam.at.hw15.pages.RegistrationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Registration {
    private static By locator_entry = By.id("mailbox:submit");

    public MainPage entry(WebDriver driver, User user) {
        new RegistrationPage()
                .enterUserName(user.getLogin())
                .enterUserDomain(user.getDomain())
                .enterUserPassword(user.getPassword())
                .clickLoginButton();
        return new MainPage();
    }
}
