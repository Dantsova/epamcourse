package com.epam.at.hw15.pages;

import com.epam.at.hw15.patterns.SingltonWebDriver;
import org.openqa.selenium.By;

import static com.epam.at.hw15.utils.Expectations.waitForElementClickability;
import static com.epam.at.hw15.utils.Expectations.waitForElementVisibility;

public class RegistrationPage extends Page {

    private static By locator_userMailName = By.id("mailbox:login");
    private static By locator_userMailDomain = By.id("mailbox:domain");
    private static By locator_userMailPassword = By.id("mailbox:password");
    private static By locator_btn_logIn = By.id("mailbox:submit");

    public RegistrationPage enterUserName(String login) {
        waitForElementVisibility(SingltonWebDriver.getInstance(), locator_userMailName);
        SingltonWebDriver.getInstance().findElement(locator_userMailName).clear();
        SingltonWebDriver.getInstance().findElement(locator_userMailName).sendKeys(login);
        return this;
    }

    public RegistrationPage enterUserDomain(String domain) {
        SingltonWebDriver.getInstance().findElement(locator_userMailDomain);
        SingltonWebDriver.getInstance().findElement(locator_userMailDomain).sendKeys(domain);
        return this;
    }

    public RegistrationPage enterUserPassword(String password) {
        waitForElementVisibility(SingltonWebDriver.getInstance(), locator_userMailPassword);
        SingltonWebDriver.getInstance().findElement(locator_userMailPassword).clear();
        SingltonWebDriver.getInstance().findElement(locator_userMailPassword).sendKeys(password);
        return this;
    }

    public MainPage clickLoginButton() {
        waitForElementClickability(SingltonWebDriver.getInstance(), locator_btn_logIn);
        SingltonWebDriver.getInstance().findElement(locator_btn_logIn).click();
        return new MainPage();
    }
}