package com.epam.at.hw15.pages;

import com.epam.at.hw15.patterns.SingltonWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DraftPage extends Page {

    private final By locator_dataListDraft = By.xpath("//div[@class='b-datalist__item__panel']/div[@class='b-datalist__item__info']");
    private By locator_dataListDraft_address = By.className("b-datalist__item__addr");
    private By locator_dataListDraft_body = By.className("b-datalist__item__subj__snippet");

    public boolean findLetterInDraft(String address, String mailBody) {
        boolean status = false;
        List <WebElement> elements = SingltonWebDriver.getInstance().findElements(locator_dataListDraft);
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).findElement(locator_dataListDraft_address).getText().equals(address) &&
                    elements.get(i).findElement(locator_dataListDraft_body).getText().equals(mailBody)) {
                elements.get(i).click();
                status = true;
                break;
            }
        }
        return status;
    }
}