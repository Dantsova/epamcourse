package com.epam.at.hw15.pages;

import com.epam.at.hw15.patterns.SingltonWebDriver;
import org.openqa.selenium.By;

public class Page {
    public boolean elementIsPresent(By locator) {
        return SingltonWebDriver.getInstance().findElements(locator).size() > 0;
    }
}