package com.epam.at.hw15.pages;

import com.epam.at.hw15.utils.Expectations;
import com.epam.at.hw15.patterns.SingltonWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.UUID;

public class CreateNewLetterPage extends Page {
    private String subject = UUID.randomUUID().toString();

    private final By locator_inputAddress = By.cssSelector("textarea[data-original-name='To']");
    private final By locator_inputSubject = By.cssSelector("input[name='Subject']");
    private final By locator_inputMailBody = By.id("tinymce");
    private final By locator_btn_saveDraft = By.xpath("//div[@class='b-toolbar']//div[@data-name='saveDraft']");
    private final By locator_btn_sent = By.xpath("//div[@class='b-toolbar']//div[@data-name='send']");
    private final By locator_message_savedInDrafts = By.xpath("//div[@class='b-toolbar__item']//div[@data-mnemo='saveStatus']");
    private final By locator_message_sendLetter = By.xpath("//div[@class='message-sent__title']");

    public CreateNewLetterPage enterMailAddress(String address) {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_inputAddress);
        SingltonWebDriver.getInstance().findElement(locator_inputAddress).clear();
        SingltonWebDriver.getInstance().findElement(locator_inputAddress).sendKeys(address);
        return this;
    }

    public CreateNewLetterPage enterMailSubject() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_inputSubject);
        SingltonWebDriver.getInstance().findElement(locator_inputSubject).clear();
        SingltonWebDriver.getInstance().findElement(locator_inputSubject).sendKeys(subject);
        return this;
    }

    public CreateNewLetterPage enterMailBody(String mailBody) {
        WebElement iFrame = SingltonWebDriver.getInstance().findElement(By.tagName("iframe"));
        SingltonWebDriver.getInstance().switchTo().frame(iFrame);
        SingltonWebDriver.getInstance().findElement(locator_inputMailBody).clear();
        SingltonWebDriver.getInstance().findElement(locator_inputMailBody).sendKeys(mailBody);
        SingltonWebDriver.getInstance().switchTo().defaultContent();
        return this;
    }

    public CreateNewLetterPage clickOnSaveDraftButton() {
        SingltonWebDriver.getInstance().findElement(locator_btn_saveDraft).click();
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_message_savedInDrafts);
        return this;
    }

    public boolean isVisibleSaveStatus() {
        return elementIsPresent(locator_message_savedInDrafts);
    }

    public CreateNewLetterPage clickOnSendButton() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_btn_sent);
        SingltonWebDriver.getInstance().findElement(locator_btn_sent).click();
        return this;
    }

    public boolean isVisibleSendStatus() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_message_sendLetter);
        return elementIsPresent(locator_message_sendLetter);
    }
}