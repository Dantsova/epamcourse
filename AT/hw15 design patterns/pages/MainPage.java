package com.epam.at.hw15.pages;

import com.epam.at.hw15.utils.Expectations;
import com.epam.at.hw15.patterns.SingltonWebDriver;
import org.openqa.selenium.By;

public class MainPage extends Page {

    private final By locator_login = By.id("PH_authLink");
    private final By locator_userBoxName = By.cssSelector("#PH_user-email");
    private final By locator_btn_writeTheLetter = By.cssSelector("#b-toolbar__left a[data-name='compose']");
    private final By locator_link_sent = By.xpath("////div[@data-mnemo='nav-folders']/div[3]");
    private final By locator_link_draft = By.xpath("//div[@data-mnemo='nav-folders']/div[4]");
    private final By lacator_draft_label_active = By.xpath("//*[contains(@class,'b-nav__item_active')]//span");
    private final By locator_link_logOut = By.cssSelector("#PH_logoutLink");

    public String getUserMailBoxName() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_userBoxName);
        return SingltonWebDriver.getInstance().findElement(locator_userBoxName).getText();
    }

    public CreateNewLetterPage clickOnWriteLetter() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_btn_writeTheLetter);
        SingltonWebDriver.getInstance().findElement(locator_btn_writeTheLetter).click();
        return new CreateNewLetterPage();
    }

    public DraftPage clickOnDraftLink() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_link_draft);
        SingltonWebDriver.getInstance().findElement(locator_link_draft).click();
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), lacator_draft_label_active);
        return new DraftPage();
    }

    public SentPage clickOnSentLink() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_link_sent);
        SingltonWebDriver.getInstance().findElement(locator_link_sent).click();
        return new SentPage();
    }

    public void clickOnLogOutLink() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_link_logOut);
        SingltonWebDriver.getInstance().findElement(locator_link_logOut).click();
    }

    public boolean logOutSuccessful() {
        Expectations.waitForElementVisibility(SingltonWebDriver.getInstance(), locator_login);
        return elementIsPresent(locator_login);
    }
}