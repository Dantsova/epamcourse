package com.epam.at.hw15.tests;

import com.epam.at.hw15.patterns.SingltonWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class ConfigurationTest {

    @BeforeClass
    public static void setUp() {
        SingltonWebDriver.getInstance().manage().window().maximize();
        SingltonWebDriver.getInstance().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown() {
        SingltonWebDriver.getInstance().quit();
    }
}