package com.epam.at.hw15.tests;

import com.epam.at.hw15.bisinessObject.User;
import com.epam.at.hw15.pages.*;
import com.epam.at.hw15.services.Creation;
import com.epam.at.hw15.services.Registration;
import com.epam.at.hw15.utils.Constants;
import com.epam.at.hw15.patterns.UserFactory;
import com.epam.at.hw15.patterns.SingltonWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MailTest extends ConfigurationTest {
    Creation creation = new Creation();
    Registration registration = new Registration();

    RegistrationPage registrationPage;
    CreateNewLetterPage createNewLetterPage;
    DraftPage draftPage;
    SentPage sentPage;
    MainPage mainPage;
    User user;

    @BeforeMethod
    public void init() {
        user = UserFactory.generateUniqueUser();
        SingltonWebDriver.getInstance().get(Constants.URL);

        registrationPage = new RegistrationPage();
        createNewLetterPage = new CreateNewLetterPage();
        draftPage = new DraftPage();
        sentPage = new SentPage();
    }

    @Test
    public void logInExistUserTest() {
       user = UserFactory.generateUniqueUser();
       mainPage = registration.entry(SingltonWebDriver.getInstance(), user);

        Assert.assertEquals(mainPage.getUserMailBoxName(), user.getLogin() + user.getDomain());
        mainPage.clickOnLogOutLink();
    }

    @Test
    public void logInNotExistUserTest() {
        user = UserFactory.generateUniqueUserWithLogin("Kim");
        mainPage = registration.entry(SingltonWebDriver.getInstance(), user);

        Assert.assertEquals(mainPage.getUserMailBoxName(), user.getLogin() + user.getDomain());
        mainPage.clickOnLogOutLink();
    }

    @Test
    public void createDraftLetterTest() {
        mainPage = registration.entry(SingltonWebDriver.getInstance(), user);

        createNewLetterPage = mainPage.clickOnWriteLetter();
        creation.createNewLetter(SingltonWebDriver.getInstance(), Constants.ADDRESS, Constants.MAIL_BODY)
                .clickOnSaveDraftButton();

        draftPage = mainPage.clickOnDraftLink();
        Assert.assertTrue(draftPage.findLetterInDraft(Constants.ADDRESS, Constants.MAIL_BODY));
        mainPage.clickOnLogOutLink();
    }

    @Test
    public void sentDraftLetter_checkedSentExist_Test() {
        mainPage = registration.entry(SingltonWebDriver.getInstance(), user);

        createNewLetterPage = mainPage.clickOnWriteLetter();
        creation.createNewLetter(SingltonWebDriver.getInstance(), Constants.ADDRESS, Constants.MAIL_BODY)
                .clickOnSaveDraftButton();

        draftPage = mainPage.clickOnDraftLink();
        draftPage.findLetterInDraft(Constants.ADDRESS, Constants.MAIL_BODY);

        createNewLetterPage.clickOnSendButton();

        Assert.assertTrue(sentPage.findLetterInSent(Constants.ADDRESS, Constants.MAIL_BODY));
        mainPage.clickOnLogOutLink();
    }

    @Test
    public void sentDraftLetter_checkedDraftNotExist_Test() {
        mainPage = registration.entry(SingltonWebDriver.getInstance(), user);

        createNewLetterPage = mainPage.clickOnWriteLetter();
        creation.createNewLetter(SingltonWebDriver.getInstance(), Constants.ADDRESS, Constants.MAIL_BODY)
                .clickOnSaveDraftButton();

        draftPage = mainPage.clickOnDraftLink();
        draftPage.findLetterInDraft(Constants.ADDRESS, Constants.MAIL_BODY);

        createNewLetterPage.clickOnSendButton();

        Assert.assertFalse(draftPage.findLetterInDraft(Constants.ADDRESS, Constants.MAIL_BODY));

        mainPage.clickOnLogOutLink();
    }

    @Test
    public void LogOutTest() {
        registration.entry(SingltonWebDriver.getInstance(), user)
                .clickOnLogOutLink();

        Assert.assertEquals(mainPage.logOutSuccessful(), true);
    }
}