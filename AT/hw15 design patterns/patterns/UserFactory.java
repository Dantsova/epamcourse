package com.epam.at.hw15.patterns;

import com.epam.at.hw15.bisinessObject.User;

public class UserFactory {

    public static User generateUniqueUser() {
        String l = System.currentTimeMillis() + "";
        return new User("snegurka.belovegskaya", "@mail.ru", "SNovimGodom!");
    }

    public static User generateUniqueUserWithLogin(String login) {
        String l = System.currentTimeMillis() + "";
        return new User(login, "mail.ru", "111");
    }
}
