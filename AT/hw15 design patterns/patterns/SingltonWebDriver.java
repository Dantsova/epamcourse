package com.epam.at.hw15.patterns;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

public class SingltonWebDriver {
    private static WebDriver driver;

    private SingltonWebDriver() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            WebDriverManager.getInstance(CHROME).setup();
            driver = new ChromeDriver();
        }
        return driver;
    }
}