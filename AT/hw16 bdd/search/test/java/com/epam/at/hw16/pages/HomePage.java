package com.epam.at.hw16.pages;

import org.openqa.selenium.By;

public class HomePage extends AbstractPage {
    private static final String BASE_URL = "https://www.ebay.com/";
    private final By searchInput = By.xpath("//div[@id='gh-ac-box']//input");
    private final By submit = By.xpath("//input[@id='gh-btn']");

    public HomePage open() {
        driver.get(BASE_URL);
        return this;
    }

    public HomePage enterQuery(String query) {
        waitForElementVisible(searchInput);
        driver.findElement(searchInput).sendKeys(query);
        return new HomePage();
    }

    public ResultPage clickSubmit() {
        waitForElementVisible(submit);
        driver.findElement(submit).click();
        return new ResultPage();
    }
}