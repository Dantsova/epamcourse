package com.epam.at.hw16.pages;

import org.openqa.selenium.By;

public class ResultPage extends AbstractPage {
    private final By listOfProducts = By.xpath("//*[@id='ListViewInner']//h3/a");

    public ProductPage clickOnFirstProduct() {
        driver.findElements(listOfProducts).get(0).click();
        return new ProductPage();
    }

    public String getTextFromFirstProduct() {
        return driver.findElements(listOfProducts).get(0).getText();
    }
}
