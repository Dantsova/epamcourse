package com.epam.at.hw16.steps;

import com.epam.at.hw16.browser.Browser;
import com.epam.at.hw16.pages.HomePage;
import com.epam.at.hw16.pages.ResultPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class Search extends Browser {
    @Given("^I opened Ebay.com page$")
    public void iOpenedSitePage() {
        new HomePage().open();
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new HomePage().enterQuery(query).clickSubmit();
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) {
        assertThat(new ResultPage().getTextFromFirstProduct(), containsString(expectPhrase));
    }


}
