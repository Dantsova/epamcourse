@great
Feature: Search product

  Scenario:Running a Full Text Quick Search
    Given I opened Ebay.com page
    When I search the product "часы"
    Then the term query "часы" should be the first in the Search Result grid

  Scenario Outline: Running a Full Text Quick Search
    Given I opened Ebay.com page
    When I search the product "<request>"
    Then the term query "<request>" should be the first in the Search Result grid

    Examples:
      | request |
      | Nokia   |
      | Xiaomi  |