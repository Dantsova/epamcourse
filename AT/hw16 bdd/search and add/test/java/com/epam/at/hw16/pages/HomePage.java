package com.epam.at.hw16.pages;

import org.openqa.selenium.By;

public class HomePage extends AbstractPage {
    private static final String BASE_URL = "https://www.ebay.com/";
    private final By submit = By.xpath("//input[@id='gh-btn']");

    public HomePage open() {
        driver.get(BASE_URL);
        return this;
    }

    public SearchPage clickSubmit() {
        waitForElementVisible(submit);
        driver.findElement(submit).click();
        return new SearchPage();
    }
}