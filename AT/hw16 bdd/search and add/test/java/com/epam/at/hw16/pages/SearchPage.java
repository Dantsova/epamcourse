package com.epam.at.hw16.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchPage extends AbstractPage {
    private final By listOfProducts = By.xpath("//*[@id='ListViewInner']//h3/a");

    public ProductPage clickOnFirstProduct() {
        waitForElementVisible(listOfProducts);
        List <WebElement> elements = driver.findElements(listOfProducts);
        elements.get(0).click();
        return new ProductPage();
    }

    public String getTextFromFirstProduct() {
        waitForElementVisible(listOfProducts);
        List <WebElement> elements = driver.findElements(listOfProducts);
        String q =  elements.get(0).getAttribute("data-mtdes");
        return q;
    }
}
