package com.epam.at.hw16.pages;

import org.openqa.selenium.By;

public class ProductPage extends AbstractPage {

    private final By buttonAddToCart = By.id("isCartBtn_btn");
    private final By linkTitleProduct = By.xpath("//*[@class='s-item__info clearfix']//a/h3");

    public String getTextFromNameProduct() {
        waitForElementVisible(linkTitleProduct);
        return driver.findElement(linkTitleProduct).getText();
    }

    public CartPage clickOnAddtoCardButton() {
        waitForElementVisible(buttonAddToCart);
        driver.findElement(buttonAddToCart).click();
        return new CartPage();
    }
}