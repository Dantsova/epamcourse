package com.epam.at.hw16.pages;

import org.openqa.selenium.By;

public class CartPage extends AbstractPage {
    private final By listOfProducts = By.xpath("//span[@class='BOLD']");

    public String getTextFromFirstProduct() {
        String nameOfProduct = driver.findElements(listOfProducts).get(0).getText();
        return nameOfProduct;
    }
}
