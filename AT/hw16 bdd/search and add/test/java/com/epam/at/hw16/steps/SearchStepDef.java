package com.epam.at.hw16.steps;

import com.epam.at.hw16.browser.Browser;
import com.epam.at.hw16.pages.HomePage;
import com.epam.at.hw16.pages.ProductPage;
import com.epam.at.hw16.pages.SearchPage;
import com.epam.at.hw16.services.EnterQueryServices;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class SearchStepDef extends Browser {
    @Given("^I opened Ebay.com page$")
    public void iOpenedSitePage() {
        new HomePage().open();
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new EnterQueryServices().enterQuery(query);
        new HomePage().clickSubmit();
    }

    @Then("^The term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) {
        assertThat(new SearchPage().getTextFromFirstProduct(), containsString(expectPhrase));
    }
}
