package com.epam.at.hw16;

import com.epam.at.hw16.browser.Browser;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        strict = true,
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }, tags = {"@great"}
)
public class RunAcceptanceTest {

    @AfterClass
    public static void closeDriver() {
        Browser.close();
    }
}
