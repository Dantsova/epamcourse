package com.epam.at.hw16.services;

import com.epam.at.hw16.browser.Browser;
import com.epam.at.hw16.pages.AbstractPage;
import com.epam.at.hw16.pages.HomePage;
import org.openqa.selenium.By;

public class EnterQueryServices extends AbstractPage {

    private final By searchInput = By.xpath("//div[@id='gh-ac-box']//input");

    public void enterQuery(String query) {
        waitForElementVisible(searchInput);
        driver.findElement(searchInput).sendKeys(query);
    }

}
