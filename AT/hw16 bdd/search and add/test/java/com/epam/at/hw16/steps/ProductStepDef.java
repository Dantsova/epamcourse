package com.epam.at.hw16.steps;

import com.epam.at.hw16.browser.Browser;
import com.epam.at.hw16.pages.CartPage;
import com.epam.at.hw16.pages.HomePage;
import com.epam.at.hw16.pages.ProductPage;
import com.epam.at.hw16.pages.SearchPage;
import com.epam.at.hw16.services.EnterQueryServices;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class ProductStepDef extends Browser {

    @When("^I add the product \"([^\"]*)\" to cart$")
    public void iAddProductToCart(String expectTitle) {
        new SearchPage().clickOnFirstProduct().clickOnAddtoCardButton();
    }

    @Then("^The term \"([^\"]*)\" should be the first in the Сart in the list of goods$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectTitle) {
        assertThat(new CartPage().getTextFromFirstProduct().toLowerCase(), containsString(expectTitle.toLowerCase()));
    }
}

