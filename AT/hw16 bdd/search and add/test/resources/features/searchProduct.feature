@great
Feature: Search product

  Scenario: Running a Full Text Quick Search
    Given I opened Ebay.com page
    When I search the product "chair"
    Then The term query "chair" should be the first in the Search Result grid

