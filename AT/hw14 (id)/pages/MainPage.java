package com.epam.at.hw14.pages;

import com.epam.at.hw14.utils.Expectations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends Page {

    private final By locator_login = By.id("PH_authLink");
    private final By locator_userBoxName = By.cssSelector("#PH_user-email");
    private final By locator_btn_writeTheLetter = By.cssSelector("#b-toolbar__left a[data-name='compose']");
    private final By locator_link_sent = By.xpath("////div[@data-mnemo='nav-folders']/div[3]");
    private final By locator_link_draft = By.xpath("//div[@data-mnemo='nav-folders']/div[4]");
    private final By lacator_draft_label_active = By.xpath("//*[contains(@class,'b-nav__item_active')]//span");
    private final By locator_link_logOut = By.cssSelector("#PH_logoutLink");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public String getUserMailBoxName() {
        Expectations.waitForElementVisibility(driver, locator_userBoxName);
        return driver.findElement(locator_userBoxName).getText();
    }

    public CreateNewLetterPage clickOnWriteLetter() {
        Expectations.waitForElementVisibility(driver, locator_btn_writeTheLetter);
        driver.findElement(locator_btn_writeTheLetter).click();
        return new CreateNewLetterPage(driver);
    }

    public DraftPage clickOnDraftLink() {
        Expectations.waitForElementVisibility(driver, locator_link_draft);
        driver.findElement(locator_link_draft).click();
        Expectations.waitForElementVisibility(driver, lacator_draft_label_active);
        return new DraftPage(getDriver());
    }

    public SentPage clickOnSentLink() {
        Expectations.waitForElementVisibility(driver, locator_link_sent);
        driver.findElement(locator_link_sent).click();
        return new SentPage(getDriver());
    }

    public void clickOnLogOutLink() {
        Expectations.waitForElementVisibility(driver, locator_link_logOut);
        driver.findElement(locator_link_logOut).click();
    }

    public boolean logOutSuccessful() {
        Expectations.waitForElementVisibility(driver, locator_login);
        return elementIsPresent(locator_login);
    }
}