package com.epam.at.hw14.pages;

import com.epam.at.hw14.utils.Expectations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DraftPage extends Page {

    private final By locator_dataListDraft = By.xpath("//form[@action='/compose/']");
//    private By locator_dataListDraft_address = By.className("b-datalist__item__addr");
//    private By locator_dataListDraft_body = By.className("b-datalist__item__subj__snippet");

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public boolean findLetterInDraft(String id) {
       //Expectations.waitForElementVisibility(driver, locator_dataListDraft);
        boolean status = false;
        List <WebElement> elements = driver.findElements(locator_dataListDraft);
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).findElement(locator_dataListDraft_address).getText().equals(address) &&
                    elements.get(i).findElement(locator_dataListDraft_body).getText().equals(mailBody)) {
                elements.get(i).click();
                status = true;
                break;
            }
        }
        return status;
    }
}
