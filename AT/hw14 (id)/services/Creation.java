package com.epam.at.hw14.services;

import com.epam.at.hw14.pages.CreateNewLetterPage;
import com.epam.at.hw14.pages.Page;
import org.openqa.selenium.WebDriver;

public class Creation {
    CreateNewLetterPage createNewLetterPage;


    public CreateNewLetterPage createNewLetter(WebDriver driver, String adress, String mailBody) {
        createNewLetterPage = new CreateNewLetterPage(driver);
        createNewLetterPage.enterMailAddress(adress)
                           .enterMailSubject()
                           .enterMailBody(mailBody);
        return new CreateNewLetterPage(driver);
    }
}
