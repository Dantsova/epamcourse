package com.epam.at.hw14.services;

import com.epam.at.hw14.pages.CreateNewLetterPage;
import com.epam.at.hw14.pages.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateLetterService {
    CreateNewLetterPage createNewLetterPage;
    private By locator_formNewLetter = By.xpath("//form[@action='/compose/']");

    public String createNewLetter(WebDriver driver, String adress, String mailBody) {
        createNewLetterPage = new CreateNewLetterPage(driver);
        createNewLetterPage.enterMailAddress(adress)
                .enterMailSubject()
                .enterMailBody(mailBody);
        return driver.findElement(locator_formNewLetter).getAttribute("id");
    }
}
