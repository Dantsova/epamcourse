package com.epam.at.hw14.bisinessObject;

public class User {
    private String login;
    private String domain;
    private String password;

    public User(String login,String domain,String password) {
        this.login = login;
        this.domain = domain;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getDomain() {
        return domain;
    }

    public String getPassword() { return password;
    }
}
